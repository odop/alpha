<?php

namespace AdminBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use OdopData\Entity\Classified;
use OdopData\Entity\User;

class CommentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('content')
            ->add('rate')
            ->add('user', EntityType::class, array(
                'class'    => User::class,
                'placeholder' => 'Choisir un auteur',
                'choice_label' => 'lastname'))
            ->add('classified', EntityType::class, array(
                'class'    => Classified::class,
                'placeholder' => 'Choisir une annonce',
                'choice_label' => 'title'))
            ->add('status', ChoiceType::class, array(
                'choices'  => array(
                    'En Attente' => 'pending',
                    'Validé' => 'valide',
                    'Supprimé' => 'deleted',
                ),
            ));
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'OdopData\Entity\Comment'
        ));
    }
}
