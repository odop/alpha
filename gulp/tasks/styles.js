var gulp         = require('gulp');
var paths        = require('../config');
var gutil        = require('gulp-util');
var sass         = require('gulp-sass');
var plumber      = require('gulp-plumber');
var autoprefixer = require('autoprefixer');
var postcss      = require('gulp-postcss');
var postURL      = require('postcss-url');
var url          = require('url');
var reload       = require('browser-sync').reload;
// require('es6-promise').polyfill();

// compile all sass files
gulp.task('styles', function() {
  return gulp.src(paths.styles.main)
    .pipe(plumber({ errorHandler: onError}))
    .pipe(sass())
    .pipe(postcss([
      postURL({ url: bust }),
      autoprefixer({ browsers: ['last 4 version', 'Firefox 20'] })
    ]))
    .pipe(plumber.stop())
    .pipe(gulp.dest(paths.styles.to))
    .pipe(reload({ stream: true })); // auto-inject into browsers
});

// When an error occurs, display in red and beep
function onError(err) {
  gutil.beep();
  console.log(gutil.colors.red(err.message));
  this.emit('end');
}

function bust(path) {
  var asset = url.parse(path);
  asset.query = asset.query || {};
  asset.query.version = require('../../package.json').version;
  return url.format(asset);
}
