# Gulp tasks

Strongly inspired by [https://github.com/greypants/gulp-starter](https://github.com/greypants/gulp-starter)

##Folder organisation

- `gulpfile.js`
- `gulp`
  - `config.js`
  - `tasks`
    - `assets.js`
    - `browserSync.js`
    - `clean.js`
    - `default.js`
    - `deploy.js`
    - `functions.js`
    - `lint.js`
    - `scripts.js`
    - `styles.js`
    - `watch.js`
    
### Paths & configuration
- `/gulp/config.js`

### Assets
- `clean`
- `assets`
- `scripts`
- `browser-sync`
- `bs-reload`
- `watch`

### Deployment
- `build`
- `push`
- `bump`
- `jenkins`

### Linter
- `lint`

## Common tasks to use
- `deploy`
- `default`
