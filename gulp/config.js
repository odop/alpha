module.exports = {

  node: {
    all: './app/**/*.js'
  },

  scripts: {
    main: ['./src/AppBundle/Resources/assets/index.js', './src/AdminBundle/Resources/assets/index.js'],
    all: './src/**/*.js',
    to: './web/assets/',
    tofile: 'index.js'
  },

  styles: {
    main: ['./src/AppBundle/Resources/assets/*.scss', './src/AdminBundle/Resources/assets/*.scss'],
    all: './src/**/*.scss',
    to: './web/assets/'
  },

  assets: {
    all: ['./src/**/*.{png,jpg,eot,svg,ttf,woff,woff2,ico,txt}', './node_modules/font-awesome/**/*.{eot,svg,ttf,woff,woff2,otf}'],
    to: './web/assets/'
  },

};
