<?php

namespace OdopData\Entity;

use OdopData\Entity\Equipment;
use OdopData\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Classified
 *
 * @ORM\Table(name="classified")
 * @ORM\Entity(repositoryClass="OdopData\Repository\ClassifiedRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Classified
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=255)
     */
    private $street;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="location_lat", type="string", length=255, nullable=true)
     */
    private $location_lat;

    /**
     * @var string
     *
     * @ORM\Column(name="location_lng", type="string", length=255, nullable=true)
     */
    private $location_lng;

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=255, nullable=true)
     */
    private $region;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255, nullable=true)
     */
    private $country;


    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="surface_area", type="integer")
     */
    private $surfaceArea;

    /**
     * @var int
     *
     * @ORM\Column(name="max_participant", type="integer")
     */
    private $maxParticipant;

    /**
     * @var int
     *
     * @ORM\Column(name="price", type="integer")
     */
    private $price;

    /**
     * @var \DateTime
     *@Gedmo\Timestampable(on="create")
     * @ORM\Column(name="date_created", type="datetime")
     */
    private $dateCreated;

    /**
     * @var \DateTime
     *@Gedmo\Timestampable(on="update")
     * @ORM\Column(name="date_modified", type="datetime", nullable=true)
     */
    private $dateModified;

    /**
     * @ORM\ManyToMany(targetEntity="OdopData\Entity\Category", cascade={"persist"}, inversedBy="classifieds")
     * @ORM\JoinColumn(nullable=true)
     */
    private $categorys;

    /**
     * @ORM\ManyToMany(targetEntity="OdopData\Entity\Document", cascade={"persist"})
     */
    private $pictures;

    /**
     * @ORM\ManyToMany(targetEntity="OdopData\Entity\Equipment", cascade={"persist"})
     */
    private $equipments;

    /**
     * @ORM\OneToMany(targetEntity="OdopData\Entity\Reservation", cascade={"persist"}, mappedBy="classified")
     * @ORM\JoinColumn(nullable=true)
     *
     */
    private $reservations;

    /**
     * @ORM\OneToMany(targetEntity="OdopData\Entity\Comment", cascade={"persist"}, mappedBy="classified")
     * @ORM\JoinColumn(nullable=true)
     *
     */
    private $comments;

    /**
     * @ORM\ManyToOne(targetEntity="OdopData\Entity\User", inversedBy="classifieds")
     */
    private $owner;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->categorys = new ArrayCollection();
        $this->equipments = new ArrayCollection();
        $this->pictures = new ArrayCollection();
        $this->reservations = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Classified
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return Classified
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set region
     *
     * @param string $region
     *
     * @return Classified
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Classified
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Classified
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set surfaceArea
     *
     * @param string $surfaceArea
     *
     * @return Classified
     */
    public function setSurfaceArea($surfaceArea)
    {
        $this->surfaceArea = $surfaceArea;

        return $this;
    }

    /**
     * Get surfaceArea
     *
     * @return string
     */
    public function getSurfaceArea()
    {
        return $this->surfaceArea;
    }

    /**
     * Set maxParticipant
     *
     * @param integer $maxParticipant
     *
     * @return Classified
     */
    public function setMaxParticipant($maxParticipant)
    {
        $this->maxParticipant = $maxParticipant;

        return $this;
    }

    /**
     * Get maxParticipant
     *
     * @return int
     */
    public function getMaxParticipant()
    {
        return $this->maxParticipant;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Classified
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return Classified
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set dateModified
     *
     * @param \DateTime $dateModified
     *
     * @return Classified
     */
    public function setDateModified($dateModified)
    {
        $this->dateModified = $dateModified;

        return $this;
    }

    /**
     * Get dateModified
     *
     * @return \DateTime
     */
    public function getDateModified()
    {
        return $this->dateModified;
    }

    /**
     * @ORM\PreUpdate
     */
    public function updateDate()
    {
        $this->setDateModified(new \Datetime());
    }

    /**
     * Add category
     *
     * @param Category $category
     *
     * @return Classified
     */
    public function addCategory(Category $category)
    {
        $this->categorys[] = $category;

        return $this;
    }

    /**
     * Remove category
     *
     * @param Category $category
     */
    public function removeCategory(Category $category)
    {
        $this->categorys->removeElement($category);
    }

    /**
     * Get categorys
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategorys()
    {
        return $this->categorys;
    }

    /**
     * @return mixed
     */
    public function __toString() {
        return strval($this->id);
    }

    /**
     * Set owner
     *
     * @param User $owner
     *
     * @return Classified
     */
    public function setOwner(User $owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Add equipments
     *
     * @param Equipment $equipments
     *
     * @return Classified
     */
    public function addEquipment(Equipment $equipments)
    {
        $this->equipments[] = $equipments;

        return $this;
    }

    /**
     * Remove equipments
     *
     * @param Equipment $equipments
     */
    public function removeEquipment(Equipment $equipments)
    {
        $this->equipments->removeElement($equipments);
    }

    /**
     * Get equipments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEquipments()
    {
        return $this->equipments;
    }

    /**
     * Get equipments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEquipment()
    {
        return $this->equipments;
    }

    /**
     * Add picture
     *
     * @param Document $picture
     *
     * @return Classified
     */
    public function addPicture(Document $picture)
    {
        $this->pictures[] = $picture;

        return $this;
    }

    /**
     * Remove category
     *
     * @param Document $picture
     */
    public function removePicture(Document $picture)
    {
        $this->pictures->removeElement($picture);
    }

    /**
     * Get categorys
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPictures()
    {
        return $this->pictures;
    }


    /**
     * Add reservation
     *
     * @param \OdopData\Entity\Reservation $reservation
     *
     * @return Classified
     */
    public function addReservation(Reservation $reservation)
    {
        $this->reservations[] = $reservation;

        return $this;
    }

    /**
     * Remove reservation
     *
     * @param \OdopData\Entity\Reservation $reservation
     */
    public function removeReservation(Reservation $reservation)
    {
        $this->reservations->removeElement($reservation);
    }

    /**
     * Get reservations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReservations()
    {
        return $this->reservations;
    }


    /**
     * Add comment
     *
     * @param \OdopData\Entity\Comment $comment
     *
     * @return Classified
     */
    public function addComment(Comment $comment)
    {
        $this->comments[] = $comment;

        return $this;
    }

    /**
     * Remove comment
     *
     * @param \OdopData\Entity\Comment $comment
     */
    public function removeComment(Comment $comment)
    {
        $this->comments->removeElement($reservation);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set street
     *
     * @param string $street
     *
     * @return Classified
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set locationLat
     *
     * @param string $locationLat
     *
     * @return Classified
     */
    public function setLocationLat($locationLat)
    {
        $this->location_lat = $locationLat;

        return $this;
    }

    /**
     * Get locationLat
     *
     * @return string
     */
    public function getLocationLat()
    {
        return $this->location_lat;
    }

    /**
     * Set locationLng
     *
     * @param string $locationLng
     *
     * @return Classified
     */
    public function setLocationLng($locationLng)
    {
        $this->location_lng = $locationLng;

        return $this;
    }

    /**
     * Get locationLng
     *
     * @return string
     */
    public function getLocationLng()
    {
        return $this->location_lng;
    }
}
