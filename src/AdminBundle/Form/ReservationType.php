<?php

namespace AdminBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use OdopData\Entity\Classified;
use OdopData\Entity\User;

class ReservationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateStart', DateType::class, array(
                'widget' => 'single_text',
                // do not render as type="date", to avoid HTML5 date pickers
                'html5' => false,
                // add a class that can be selected in JavaScript
                'attr' => ['data-provider' => 'date_start'],
                'format' => 'dd-MM-yyyy',
            ))
            ->add('dateEnd', DateType::class, array(
                'widget' => 'single_text',
                // do not render as type="date", to avoid HTML5 date pickers
                'html5' => false,
                // add a class that can be selected in JavaScript
                'attr' => ['data-provider' => 'date_end'],
                'format' => 'dd-MM-yyyy',
            ))
            ->add('classified', EntityType::class, array(
                'class'    => Classified::class,
                'placeholder' => 'Choisir une annonce',
                'choice_label' => 'title'))
            ->add('user', EntityType::class, array(
                'class'    => User::class,
                'placeholder' => 'Choisir un client',
                'choice_label' => 'lastname'))
            ->add('status', ChoiceType::class, array(
                'choices'  => array(
                    'En Attente' => 'pending',
                    'Validé' => 'valide',
                    'Terminé' => 'completed',
                ),
            ));
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'OdopData\Entity\Reservation'
        ));
    }
}
