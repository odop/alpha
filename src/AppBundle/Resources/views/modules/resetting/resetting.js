export default function resetting() {

    $(document).ready(function () {
        let resetting_form = $('#resetting-form');

        $("#resetting-trigger").click(function(){
            $('#loginModal').modal('hide');
            $('#resettingModal').modal('show');
        });

        $('#resettingModal .login-submit').on("click", function (e) {
            e.preventDefault();
            $('#resettingModal .login-submit').html('<i class="fa fa-spinner fa-spin"></i>');
            $.ajax({
                type: resetting_form.attr('method'),
                url: resetting_form.attr('action'),
                data: resetting_form.serialize(),
                success: function(response) {
                    if(response.success){
                        $('#resetting-content').hide();
                        $('#resetting-success').removeClass("hidden").html(response.message);
                        setTimeout(function(){
                            $('#resetting-success').addClass("hidden").empty();
                            $('#resettingModal').modal('hide');
                        }, 3000);
                    }
                    else{
                        $('#resetting-content').hide();
                        $('#resetting-error').removeClass("hidden").html(response.message);
                        setTimeout(function(){
                            $('#resetting-error').addClass("hidden").empty();
                            $('#resettingModal').modal('hide');
                        }, 3000);
                    }
                },
                error: function(XHR, textStatus, errorThrown){
                    console.log( 'error server response: ' + textStatus + ': ' + errorThrown );
                }
            });
        });
    });
}

resetting.selector = '#resettingModal';
