<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use blackknight467\StarRatingBundle\Form\RatingType;
use AppBundle\Form\DataTransformer\NumberIdTransformer;
use Doctrine\Common\Persistence\ObjectManager;
// use Doctrine\ORM\EntityManager;
use OdopData\Entity\Comment;


class CommentType extends AbstractType
{

    /**
     * @var FormBuilderInterface $em
     */
     protected $em;

    /**
     * Constructor
     *
     * @param FormBuilderInterface $em
     */
    public function __construct(ObjectManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('rate', RatingType::class, array(
                'required' => false,
            ))
            ->add('content', TextareaType::class, array(
                'attr' => array(
                  'required',
            )))
            ->add('classified', HiddenType::class, array())
        ;

        $builder->get('classified')
            ->addModelTransformer(new NumberIdTransformer($this->em, 'OdopData:Classified'));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Comment::class,
        ));
    }

    public function getName()
    {
        return 'comment';
    }
}
