<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use OdopData\Entity\Classified;
use OdopData\Entity\Category;
use AdminBundle\Form\ClassifiedType;

/**
 * Classified controller.
 *
 * @Route("admin/classified")
 */
class ClassifiedController extends Controller
{
    /**
     * Lists all Classified entities.
     *
     * @Route("/{page}", name="admin_classified_index", defaults={"page": 1}, requirements={"page": "\d+"})
     * @Method("GET")
     */
    public function indexAction(Request $request, $page)
    {
        $this->get('admin.breadcrumbs')->setBreadcrumbs(array("admin_classified_index" => "Annonces"));

        $em = $this->getDoctrine()->getManager();
        $classifieds = $em->getRepository(Classified::class)->getClassifieds();

        $paginator  = $this->get('knp_paginator');
        $result = $paginator->paginate(
            $classifieds, /* query NOT result */
            $request->query->getInt('page', $page)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('AdminBundle:Classified:index.html.twig', array(
            'classifieds' => $result,
        ));
    }

    /**
     * Creates a new Classified entity.
     *
     * @Route("/new", name="admin_classified_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $this->get('admin.breadcrumbs')->setBreadcrumbs(
            array("admin_classified_index" => "Annonces",
                "admin_classified_new" => "Nouveau" ));

        $classified = new Classified();
        $form = $this->createForm('AdminBundle\Form\ClassifiedType', $classified);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($classified);
            $em->flush();

            $this->addFlash(
                'success',
                'Ajouté avec succès'
            );

            return $this->redirectToRoute('admin_classified_show', array('id' => $classified->getId()));
        }

        return $this->render('AdminBundle:Classified:new.html.twig', array(
            'classified' => $classified,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Classified entity.
     *
     * @Route("/{id}/show", name="admin_classified_show")
     * @Method("GET")
     * @param Classified $classified
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Classified $classified)
    {
        $this->get('admin.breadcrumbs')->setBreadcrumbs(
            array("admin_classified_index" => "Annonces",
                "admin_dashboard" => "Afficher" ));

        $deleteForm = $this->createDeleteForm($classified);
        $form = $this->createForm('AdminBundle\Form\ClassifiedType', $classified);

        return $this->render('AdminBundle:Classified:show.html.twig', array(
            'classified' => $classified,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Classified entity.
     *
     * @Route("/{id}/edit", name="admin_classified_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Classified $classified
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Classified $classified)
    {
        $this->get('admin.breadcrumbs')->setBreadcrumbs(
            array("admin_classified_index" => "Annonces",
                "admin_dashboard" => "Editer" ));

        $em = $this->getDoctrine()->getManager();
        $categorys = $em->getRepository(Category::class)->findAll();

        $deleteForm = $this->createDeleteForm($classified);
        $form = $this->createForm('AdminBundle\Form\ClassifiedType', $classified);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($classified);
            $em->flush();

            $this->addFlash(
                'success',
                'Modifications enregistrées avec succès'
            );

            return $this->redirectToRoute('admin_classified_edit', array('id' => $classified->getId()));
        }

        return $this->render('AdminBundle:Classified:edit.html.twig', array(
            'classified' => $classified,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Classified entity.
     *
     * @Route("/{id}", name="admin_classified_delete")
     * @Method("DELETE")
     * @param Request $request
     * @param Classified $classified
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Classified $classified)
    {
        $form = $this->createDeleteForm($classified);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($classified);
            $em->flush();

            $this->addFlash(
                'success',
                'Supprimé avec succès'
            );
        }

        return $this->redirectToRoute('admin_classified_index');
    }

    /**
     * Creates a form to delete a Classified entity.
     *
     * @param Classified $classified The Classified entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Classified $classified)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_classified_delete', array('id' => $classified->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
