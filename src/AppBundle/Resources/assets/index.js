// Add client side javascript here
// let jQuery = require('jquery');
window.jQuery = window.$ = require('jquery');
let bootstrap = require('bootstrap-sass');

window.moment  = require('moment');
let locale  = require('../../../../node_modules/moment/locale/fr');

moment().format();

$.fn.datetimepicker  = require('eonasdan-bootstrap-datetimepicker');

$.fn.datetimepicker.defaults.icons = {
    time: 'fa fa-clock-o',
    date: 'fa fa-calendar',
    up: 'fa fa-chevron-up',
    down: 'fa fa-chevron-down',
    previous: 'fa fa-angle-double-left',
    next: 'fa fa-angle-double-right',
    today: 'fa fa-dot-circle-o',
    clear: 'fa fa-trash',
    close: 'fa fa-times'
};


let owlCarousel  = require('owl.carousel');
window.Dropzone  = require('dropzone');
let rating  = require('../../../../vendor/blackknight467/star-rating-bundle/blackknight467/StarRatingBundle/Resources/public/js/rating');

import header       from '../views/modules/header/header';
import carousel     from '../views/modules/carousel/carousel';
import homepage     from '../views/Homepage/homepage';
import classified   from '../views/Classified/classified';
import reservation  from '../views/Reservation/reservation';
import comment      from '../views/Comment/comment';
import user         from '../views/User/user';
import login        from '../views/modules/login/login';
import signin       from '../views/modules/signin/signin';
import resetting    from '../views/modules/resetting/resetting';

Dropzone.autoDiscover = false;

jQuery(document).ready(() => {
  // loads all components
  [
    header,
    login,
    signin,
    resetting,
    carousel,
    homepage,
    classified,
    reservation,
    user,
    comment
  ].forEach(component => {
    if (jQuery(component.selector).length) component();
  });

});
