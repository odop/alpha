
export default function user() {

    let form = $("#edit_profile");
    const APPROB_VALIDATE = "Réservation validée";
    const APPROB_REFUSED = "Réservation refusée";

    function approbationReservation(status, idReservation, url, context) {

        let colorIcon = status == 'valide' ? '#00a388':'#db514f';
        let classApprobation = status == 'valide' ? 'fa-check-circle' : 'fa-close';
        let result = '<p><i class="fa '+ classApprobation + ' fa-fw" style="color:' + colorIcon +'"></i>' + (status == "valide" ? APPROB_VALIDATE : APPROB_REFUSED) + '</p>';
        let parents = context.parents('.button-approbation');
        let resultApprobation = parents.next('.result-approbation');

        $.ajax({
                type: 'POST',
                url: url,
                data: {'id' : idReservation, 'status' : status}
            })
            .done(function (data) {
                if (typeof data.success !== true) {
                    parents.hide();
                    resultApprobation.html(result);
                }
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                if (typeof jqXHR.responseJSON !== 'undefined') {
                    console.log('Erreur lors de la requête ajax')
                } else {
                    alert(errorThrown);
                }
            });
    }

    $('#approbation-valide').on('click', (function(e){
        e.preventDefault();

        let idReservation = $(this).attr('data-id');
        let context = $(this);
        let status = 'valide';
        let url = $('#path_approbation').attr('data-id');

        approbationReservation(status, idReservation, url, context);

    }));

    $('#approbation-refused').on('click', (function(e){
        e.preventDefault();

        let idReservation = $(this).attr('data-id');
        let context = $(this);
        let status = "refused";
        let url = $('#path_approbation').attr('data-id');

        approbationReservation(status, idReservation, url, context);

    }));

    $('#recapModal').on('show.bs.modal', function (e) {
        let button = $(e.relatedTarget);
        let idReservation = button.data('id');
        let url = button.data('url');
        let context = $(this);

        $.ajax({
                type: 'POST',
                url: url,
                data: {id : idReservation}
            })
            .done(function (data) {
                if (typeof data.success !== true) {
                    $('#price').html(data.reservation.price + "€");
                    $('#dateStart').html(data.reservation.dateStart);
                    $('#dateEnd').html(data.reservation.dateEnd);
                    $('#participant').html(data.reservation.participant);
                    $('.container-button-paypal').html('<div id="paypal-button"></div>');
                    renderPaypalButton(data.reservation.price, button, idReservation, url, context);
                }
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                if (typeof jqXHR.responseJSON !== 'undefined') {
                    console.log('Erreur lors de la requête ajax')
                } else {
                    alert(errorThrown);
                }
            });
    });


    function renderPaypalButton(price, button, idReservation, url, context) {
        paypal.Button.render({

            env: 'sandbox', // sandbox | production

            client: {
                sandbox:    'Adz_rAN-V0j4swIp0SIYPJm1Mnxa9BBbXNBDuOubZo4v0ODKfluiHl327k_Nc3ti1bhLO6n65zXhG7ct'
                //production: '<insert production client id>'
            },

            // Show the buyer a 'Pay Now' button in the checkout flow
            commit: true,

            // payment() is called when the button is clicked
            payment: function(data, actions) {

                // Make a call to the REST api to create the payment
                return actions.payment.create({
                    payment: {
                        transactions: [
                            {
                                amount: { total: price, currency: 'EUR' }
                            }
                        ]
                    }
                });
            },

            // onAuthorize() is called when the buyer approves the payment
            onAuthorize: function(data, actions) {
                // Make a call to the REST api to execute the payment
                return actions.payment.execute().then(function() {
                    $.ajax({
                            type: 'POST',
                            url: url,
                            data: {id: idReservation, isPayed : true}
                        })
                        .done(function (data) {
                            if (typeof data.success !== true) {
                                context.modal('hide');
                                button.hide();
                                button.prev().html('<p><i class="fa fa-money fa-fw" style="color:#00a388"></i>Réservation payée</p>');
                            }
                        })
                        .fail(function (jqXHR, textStatus, errorThrown) {
                            if (typeof jqXHR.responseJSON !== 'undefined') {
                                console.log('Erreur lors de la requête ajax')
                            } else {
                                alert(errorThrown);
                            }
                        });
                });
            }

        }, '#paypal-button');
    }

    window.myDropzone = new Dropzone("#edit_profile", { // The camelized version of the ID of the form element

        // The configuration we've talked about above
        paramName: 'file',
        clickable:'.dz-upload',
        previewsContainer:'.dz-upload',
        autoProcessQueue: false,
        // uploadMultiple: true,
        addRemoveLinks: true,
        dictRemoveFileConfirmation: 'Supprimer cette image ?',
        parallelUploads: 100,
        maxFiles: 100,

        // The setting up of the dropzone
        init: function() {

            var myDropzone = this;

            // First change the button to actually tell Dropzone to process the queue.
            this.element.querySelector("input[type=submit]").addEventListener("click", function(e) {
                // Make sure that the form isn't actually being sent.
                if(form[0].checkValidity()) {
                    e.preventDefault();
                    e.stopPropagation();

                    if (myDropzone.getQueuedFiles().length > 0) {
                        myDropzone.processQueue();
                    } else {

                        $.ajax({
                            type: form.attr('method'),
                            url: form.attr('action'),
                            data: form.serialize(),
                            success: function(response) {
                                window.location.href = response.redirect;
                            }
                        });
                    }
                }
            });

            this.on("addedfile", function(file) {
                $(".dz-preview:last-child").attr('data-id', file.id);

                $(".dz-message").hide();
            }),

                this.on("removedfile", function(file) {

                    $.ajax({
                        type: 'get',
                        url: file.delUrl,
                        success: function(response) {
                            if(response.success) {
                                form.hide();
                                $('#comment-alert .alert-success').removeClass("hidden");
                                $('#comment-alert .alert-success .alert-message').html(response.message);
                            } else {
                                $('#comment-alert .alert-danger').removeClass("hidden");
                                $('#comment-alert .alert-danger .alert-message').html(response.message);
                            }
                        }
                    });
                }),

                this.on("removededfile", function() {
                    // $(".dz-message").show();
                }),

                // Listen to the sendingmultiple event. In this case, it's the sendingmultiple event instead
                // of the sending event because uploadMultiple is set to true.
                this.on("sending", function(file, xhr, formData, e) {
                    // Gets triggered when the form is actually being sent.
                    // Hide the success button or the complete form.

                    $.each( form.serializeArray(), function( index, value ){
                        formData.append(value['name'], value['value']);
                    });

                });
            this.on("success", function(files, response) {
                // Gets triggered when the files have successfully been sent.
                // Redirect user or notify of success.
                setTimeout(function(){
                    window.location.href = response.redirect;
                }, 2000);
            });
            this.on("error", function(files, response) {
                // Gets triggered when there was an error sending the files.
                // Maybe show form again, and notify user of error
                console.log(response);
            });

        },

    });

}

user.selector = '#user-profile';
