export default function header() {

  $(document).ready(function () {

    var hmargin = 80;
    var header = $("#main-header");
    var hheader = $("#main-header").height();
    var hsearch = $("#quick-search").height();

    function toggleHeader() {
      if(hsearch) {
        if ($(window).scrollTop() > (hsearch + hmargin)) {
          header.addClass("navbar-fixed-top");
          header.removeClass("hidden");
        } else if ($(window).scrollTop() > (hheader + hmargin)) {
          header.addClass("navbar-fixed-top hidden");
        } else {
          header.removeClass("navbar-fixed-top hidden");
        }
      }
    }

    toggleHeader();

    $(window).scroll(function () {
      toggleHeader();
    });

  });

}

header.selector = '#main-header';
