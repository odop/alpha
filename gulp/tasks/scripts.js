var gulp       = require('gulp');
var browserify = require('browserify');
var uglify     = require('gulp-uglify');
var paths      = require('../config');
var babelify   = require('babelify');
var source     = require('vinyl-source-stream');
var buffer     = require('vinyl-buffer');
var gulpif     = require('gulp-if');

var reload     = require('browser-sync').reload;
var _          = require('lodash');
var gutil      = require('gulp-util');
var watchify   = require('watchify');

// bundle all scripts
gulp.task('scripts:watch', function(cb) {
  bundle({
    watch: true,
    callback: cb,
    entries: paths.scripts.main,
    output: paths.scripts.tofile,
    log: true // show logs only once
  });
});

gulp.task('scripts:build', function(cb) {
  bundle({
    watch: false,
    callback: cb,
    entries: paths.scripts.main,
    output: paths.scripts.tofile,
    log: false
  });
});

function bundle(options) {
  // variables
  var watch   = options.watch;
  var cb      = options.callback;
  var entries = options.entries;
  var output  = options.output;
  var log     = options.log; // option only to prevent displaying logs twice
  var bundler = null;

  // check if task is for dev or prod
  if (watch) {

    _.assign(watchify.args, { debug: true, entries });

    bundler = watchify(browserify(watchify.args));
    bundler.on('update', updateHandler);

    if (log) bundler.on('log', gutil.log); // on any dep update, runs the bundler

  } else {
    bundler = browserify(entries);
  }

  // transforms
  bundler.transform(babelify.configure({ presets: ['es2015', 'react'], ignore: /(dependencies)/ })); // ES6 -> ES5

  // settings are now defined, rebundle it
  function rebundle(callback) {
    return bundler.bundle()
      .on('error', errorHandler)
      .pipe(source(output))
      .pipe(buffer())
      .pipe(gulpif(process.env.NODE_ENV === 'production', uglify()))
      .pipe(gulp.dest(paths.scripts.to))
      .on('end', callback);
  }

  function updateHandler(ids) {
    if (log) gutil.log('File changed:', gutil.colors.yellow(ids[0]));
    rebundle(reload); // send browserify reload function
  }

  function errorHandler(err) {
    gutil.log(gutil.colors.red('Browserify Error: '), err);
    this.emit('end');
  }

  // send gulp callback function
  rebundle(cb);
}
