<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class RegistrationType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('firstname', null, array('required'=> true))
			->add('lastname', null, array('required'=> true))
			->add('email', null, array('label'=> false))
			->add('plainPassword', RepeatedType::class, array(
				'type'=>PasswordType::class,
				'invalid_message' => 'Les mots de passes doivent correspondre',
    		'options' => array('attr' => array('class' => 'password-field')),
  			'required' => true,
  			'first_options'  => array('label' => false, 'attr'=>array('placeholder'=> 'Mot de passe')),
			  'second_options' => array('label' => false, 'attr'=>array('placeholder'=> 'Confirmer mot de passe')),
		   ))
			->remove('username')
		;
	}

	public function getName()
	{
		return 'app_user_registration';
	}

	public function getParent()
	{
		return 'FOS\UserBundle\Form\Type\RegistrationFormType';
	}

	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
    	$resolver->setDefaults(array(
			'data_class' => 'OdopData\Entity\User',
			'allow_extra_fields' => true,
			'csrf_token_id' => 'registration',
		));
	}
}
