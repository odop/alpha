
export default function reservation() {

  let dateStart = $('.dateStart');
  let dateEnd = $('.dateEnd');
  let participant = $('#reservation_participant');
  let participantModal = $('#reservationModal .participant');
  let price = $('#reservation .price span').text();

  /**
   * Initialisation of datetimepicker on the current page
   */
  dateStart.datetimepicker({
      locale: 'fr',
      //   disabledDates: {{ reservations|json_encode()|raw }},
      minDate: moment(),
      showClose: true,
  });

  dateEnd.datetimepicker({
      locale: 'fr',
      useCurrent: false,
      //   disabledDates: {{ reservations|json_encode()|raw }}
      showClose: true,
  });

  dateStart.on("dp.show", function (e) {
    $(".day").on('click', function(){
      $("a[data-action='togglePicker']").trigger('click');
    });
  });

  dateStart.on("dp.change", function (e) {
      dateEnd.data("DateTimePicker").minDate(e.date);
      dateStart.each(function(){
        $(this).data('DateTimePicker').date(e.date);
      });

      $(".day").on('click', function(){
        $("a[data-action='togglePicker']").trigger('click');
      });

      displayPrice(price);
  });

  dateEnd.on("dp.show", function (e) {
    $(".day").on('click', function(){
      $("a[data-action='togglePicker']").trigger('click');
    });
  });

  dateEnd.on("dp.change", function (e) {
      dateStart.data("DateTimePicker").maxDate(e.date);
      dateEnd.each(function(){
        $(this).data('DateTimePicker').date(e.date);
      });

      $(".day").on('click', function(){
        $("a[data-action='togglePicker']").trigger('click');
      });

      displayPrice(price);
  });



  participant.on("change paste keyup", function() {
     participantModal.val($(this).val());
     displayPrice(price);
  });

  /**
   *
   * @param price
   * @param isPayment
   * Get dateEnd and dateStart and calculate the price of the reservation
   */
  function displayPrice(price){

    let startDate = dateStart.data("DateTimePicker").date();
    let endDate = dateEnd.data("DateTimePicker").date();

    if(!startDate || !endDate || !price)
      return;

    let timeDiff = 0;
    if (endDate) {
        timeDiff = (endDate - startDate) / 1000;
    }
    let HoursDiff = Math.floor(timeDiff / (60 * 60));

    $('.recap-reservation .hours span').html(HoursDiff);
    $('.recap-reservation .total span').html(HoursDiff * price);

  }

    /**
    * Fire request ajax on controller Reservation to save the reservation
    */
    let form = $("#add_reservation");

    form.on("submit", function (e) {

        if(form[0].checkValidity()) {
            event.preventDefault();

            $.ajax({
                type: form.attr('method'),
                url: form.attr('action'),
                data: form.serialize(),
                success: function(response) {
                    if(response.success) {
                        form.hide();
                        $('#reservation-alert').removeClass("hidden");
                        $('#reservation-alert').addClass('alert').addClass('alert-success');
                        $('#reservation-alert').html(response.message);
                    } else {
                        $('#reservation-alert').removeClass("hidden");
                        $('#reservation-alert').addClass('alert').addClass('alert-danger');
                        $('#reservation-alert').html(response.message);
                    }
                }
            });
        }

    });
}

reservation.selector = '#reservation';
