<?php

namespace AdminBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class EquipmentControllerTest extends WebTestCase
{

    public function testCompleteScenario()
    {
        // Create a new client to browse the application
        $client = static::createClient(array(), array(
            'PHP_AUTH_USER' => 'julien.decoen@gmail.com',
            'PHP_AUTH_PW'   => 'mdp123',
        ));

        // Create a new entry in the database
        $crawler = $client->request('GET', '/admin/equipment/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET /admin/equipment/");
        $crawler = $client->click($crawler->filter('.header a')->link());

        // Fill in the form and submit it
        $form = $crawler->selectButton('Ajouter')->form(array(
            'equipment[name]'  => 'Test',
            'equipment[pictoUrl]'  => 'France',
            'equipment[category]'  => '5',
            // ... other fields to fill
        ));

        $client->submit($form);
        $crawler = $client->followRedirect();

        // Check data in the show view
        $this->assertGreaterThan(0, $crawler->filter('.alert-success:contains("Ajouté avec succès")')->count(), 'Fail to create new');

        // Edit the entity
        $crawler = $client->click($crawler->selectLink('Editer')->link());

        $form = $crawler->selectButton('Modifier')->form(array(
            'equipment[name]'  => 'Test Edit',
            // ... other fields to fill
        ));

        $client->submit($form);
        $crawler = $client->followRedirect();

        // Check the element contains an attribute with value equals "Foo"
        $this->assertGreaterThan(0, $crawler->filter('.alert-success:contains("Modifications enregistrées avec succès")')->count(), 'Fail to edit');

        // Delete the entity
        $client->submit($crawler->filter('#deleteModal')->selectButton('Supprimer')->form());
        $crawler = $client->followRedirect();

        // Check the entity has been delete on the list
        $this->assertGreaterThan(0, $crawler->filter('.alert-success:contains("Supprimé avec succès")')->count(), 'Fail to delete');

    }

}
