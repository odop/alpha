<?php

namespace AdminBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use OdopData\Entity\User;
use OdopData\Entity\Category;
use OdopData\Entity\Equipment;

class ClassifiedType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('country')
            ->add('region')
            ->add('city')
            ->add('description', TextareaType::class, array(
                'attr' => array(
                    'class' => 'tinymce',
                )))
            ->add('surfaceArea')
            ->add('maxParticipant')
            ->add('price')
            ->add('categorys', EntityType::class, array(
            'class'    => Category::class,
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('c')
                    ->where('c.type != :type')
                    ->setParameter('type', 'site_params');
            },
            'choice_label' => 'name',
            'expanded' => true,
            'label_attr' => array('class' => 'checkbox-inline'),
            'multiple' => true))
            ->add('equipment', EntityType::class, array(
            'class'    => Equipment::class,
            'choice_label' => 'name',
            'expanded' => true,
            'label_attr' => array('class' => 'checkbox-inline'),
            'multiple' => true))
            ->add('owner', EntityType::class, array(
                'class'    => User::class,
                'placeholder' => 'Choisir un propriétaire',
                'choice_label' => 'lastname'))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'OdopData\Entity\Classified',
        ));
    }
}
