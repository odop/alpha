<?php

namespace OdopData\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="OdopData\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @ORM\ManyToMany(targetEntity="OdopData\Entity\Classified", mappedBy="categorys")
     * @ORM\JoinColumn(nullable=true)
     */
    private $classifieds;

    /**
     * @ORM\ManyToOne(targetEntity="OdopData\Entity\Document")
     * @ORM\JoinColumn(nullable=true)
     */
    private $picture;

    /**
     * @var \DateTime
     *@Gedmo\Timestampable(on="create")
     * @ORM\Column(name="date_created", type="datetime")
     */
    private $dateCreated;

    /**
     * @var \DateTime
     *@Gedmo\Timestampable(on="update")
     * @ORM\Column(name="date_modified", type="datetime", nullable=true)
     */
    private $dateModified;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param \DateTime $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return \DateTime
     */
    public function getDateModified()
    {
        return $this->dateModified;
    }

    /**
     * @param \DateTime $dateModified
     */
    public function setDateModified($dateModified)
    {
        $this->dateModified = $dateModified;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Category
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->classifieds = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add classified
     *
     * @param \OdopData\Entity\Classified $classified
     *
     * @return Category
     */
    public function addClassified(\OdopData\Entity\Classified $classified)
    {
        $this->classifieds[] = $classified;

        return $this;
    }

    /**
     * Remove classified
     *
     * @param \OdopData\Entity\Classified $classified
     */
    public function removeClassified(\OdopData\Entity\Classified $classified)
    {
        $this->classifieds->removeElement($classified);
    }

    /**
     * Get classifieds
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClassifieds()
    {
        return $this->classifieds;
    }

    /**
     * @return mixed
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param mixed $picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }
}
