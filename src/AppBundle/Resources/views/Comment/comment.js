
export default function comment() {

  $(document).ready(function () {

      let form = $("#add_comment");

      form.on("submit", function (e) {



          if(form[0].checkValidity()) {
              event.preventDefault();

              // $( this ).html('<i class="fa fa-spinner fa-spin"></i>');

              $.ajax({
                  type: form.attr('method'),
                  url: form.attr('action'),
                  data: form.serialize(),
                  success: function(response) {
                      if(response.success) {
                          form.hide();
                          $('#comment-alert .alert-success').removeClass("hidden");
                          $('#comment-alert .alert-success .alert-message').html(response.message);
                      } else {
                          $('#comment-alert .alert-danger').removeClass("hidden");
                          $('#comment-alert .alert-danger .alert-message').html(response.message);
                      }
                  }
              });
          }

      });
  });
}

comment.selector = '#add_comment';
