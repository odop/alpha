$(function(){
    //je récupère l'action où sera traité l'upload en PHP
    var _actionToDropZone = $("#form_snippet_image").attr('action');

    //je définis ma zone de drop grâce à l'ID de ma div citée plus haut.
    Dropzone.autoDiscover = false;
    var myDropzone = new Dropzone("#form_snippet_image", {
        url: _actionToDropZone,
        dictDefaultMessage: 'Ajouter une photo de profil',
        autoProcessQueue: false,
    });

    myDropzone.on("addedfile", function (file) {
        $(".dz-details").append('<div class="dz-remove"><span class="fa fa-trash" aria-hidden="true"></span></div>');
        file.previewElement.addEventListener("click", function () {
            myDropzone.removeFile(file);
        });
    });

    // Setup the buttons for all transfers
    // The "add files" button doesn't need to be setup because the config
    // `clickable` has already been specified.
    document.querySelector("#actions .start").onclick = function() {
        myDropzone.processQueue();
    };
    document.querySelector("#actions .cancel").onclick = function() {
        myDropzone.removeAllFiles(true);
    };

    // Update the total progress bar
    myDropzone.on("totaluploadprogress", function(progress) {
        document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
    });

    myDropzone.on("sending", function(file, xhr, formData) {
        formData.append('target_id', $("#form_snippet_image").attr('data-target'));
        // Show the total progress bar when upload starts
        document.querySelector("#total-progress").style.opacity = "1";
        // And disable the start button
        // document.querySelector(".start").setAttribute("disabled", "disabled");
    });

    // Hide the total progress bar when nothing's uploading anymore
    // myDropzone.on("queuecomplete", function(progress) {
    //     document.querySelector("#total-progress").style.opacity = "0";
    // });

    myDropzone.on("success", function() {
        $('#uploadModal').modal('hide');
        location.reload();
    });
});