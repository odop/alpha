<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use OdopData\Entity\Category;
use OdopData\Entity\Equipment;
use OdopData\Repository\CategoryRepository;
use Ivory\GoogleMap\Place\AutocompleteComponentType;
use Ivory\GoogleMapBundle\Form\Type\PlaceAutocompleteType;
use Ivory\GoogleMap\Event\Event;

class ClassifiedType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        // Javascript variable used for your autocomplete
        $variable = 'place_autocomplete';

        // Javascript closure triggered when the user will select a place
        $handle = "function(){
            var place = ".$variable.".getPlace();
            document.getElementById('classified_city').value = place.address_components[0].long_name;
            document.getElementById('classified_region').value = place.address_components[2].long_name;
            document.getElementById('classified_country').value = place.address_components[3].long_name;
            console.log(".$variable.".getPlace().geometry.location.lat());
            console.log(".$variable.".getPlace().geometry.location.lng());
        }";

        // Create the event
        $event = new Event($variable, 'place_changed', $handle);


        $builder
            ->add('title')
            ->add('street')
            ->add('city', PlaceAutocompleteType::class, array(
              'types' => ['(cities)'],
              'components' => [AutocompleteComponentType::COUNTRY => 'fr'],
              'variable' => $variable,
              'events' => [$event],
            ))
            ->add('region', TextType::class, array(
              'disabled' => true
            ))
            ->add('country', TextType::class, array(
              'disabled' => true
            ))
            ->add('location_lat', HiddenType::class)
            ->add('location_lng', HiddenType::class)
            ->add('description', TextareaType::class, array(
                'attr' => array(
                    'rows' => '5',
                    'class' => 'tinymce',
                )))
            ->add('surfaceArea')
            ->add('maxParticipant')
            ->add('price')
            ->add('categorys', EntityType::class, array(
                'class'    => Category::class,
                'query_builder' => function (CategoryRepository $er) {
                    return $er->createQueryBuilder('c')
                    ->where('c.type = :type')->setParameter('type', 'type_event');
                },
                'choice_label' => 'name',
                'expanded' => true,
                'label_attr' => array('class' => 'checkbox-inline'),
                'multiple' => true))
            ->add('equipment', EntityType::class, array(
                'class'    => Equipment::class,
                'choice_label' => 'name',
                'expanded' => true,
                'label_attr' => array('class' => 'checkbox-inline'),
                'multiple' => true))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'OdopData\Entity\Classified',
        ));
    }
}
