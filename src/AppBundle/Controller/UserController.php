<?php

namespace AppBundle\Controller;

use OdopData\Entity\User;
use OdopData\Entity\Classified;
use OdopData\Entity\Document;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Form\RegistrationType;
use AppBundle\Form\ClassifiedType;
use Symfony\Component\Security\Core\Security;

/**
 * User controller.
 *
 */
class UserController extends Controller
{

    /**
    * Finds and displays a User entity.
    *
    * @Route("user/{id}", name="user_show")
    * @Method("GET")
    * @param User $user
    * @return \Symfony\Component\HttpFoundation\Response
    */
    public function showAction(User $user)
    {
      $em = $this->getDoctrine()->getManager();
      $userProfile = $em->getRepository('OdopData:User')->find($user->getId());
      return $this->render('AppBundle:User:show.html.twig', array(
        'user' => $userProfile,
      ));
    }

    /**
     * Finds and displays a User entity.
     *
     * @Route("/account", name="account")
     * @Method("GET")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function accountAction()
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();

        return $this->render('AppBundle:User:profile.html.twig', array(
            'user' => $user,
        ));
    }

    /**
     *
     * @Route("/account/edit/", name="account_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm('AppBundle\Form\UserType', $user);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {

          if ($form->isSubmitted() && $form->isValid()) {

              if($request->files->get('file')) {

                  $file = $request->files->get('file');
                  $document = new Document();

                  $document->setUploadDir('profile_picture/');
                  $document->setFile($file);
                  $document->setName($file->getClientOriginalName());
                  $em->persist($document);

                  $user->setProfilePicture($document);
              }

              $em->persist($user);
              $em->flush();

              $this->addFlash(
                  'success',
                  'Modifié avec succès'
              );

              return new JsonResponse(array('success' => 'Success', 'redirect' => $this->generateUrl('account_edit')), 200);
          }

          return new JsonResponse(array('error' => 'Error', 'message' => $form->getErrors()), 200);

        } else {


        return $this->render('AppBundle:User:editProfile.html.twig',array(
            'user' => $user,
            'form' => $form->createView()
        ));

        }
    }

    /**
     *
     * @Route("/account/classifieds", name="account_classified")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listClassifiedAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();

        $classifieds = $em
            ->getRepository('OdopData:User')
            ->getUserProfile($user->getId());

        return $this->render('AppBundle:User:listClassified.html.twig',array(
            'user' => $user,
            'classifieds' => $classifieds,
        ));
    }

    /**
     *
     * @Route("/account/classifieds/new", name="account_classified_new")
     * @Route("/account/classifieds/edit/{classified}", name="account_classified_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Classified $classified
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editClassifiedAction(Request $request, Classified $classified = null)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.token_storage')->getToken()->getUser();

        if($classified)
            $classified = $em->getRepository('OdopData:Classified')->getOneClassified($classified->getId());
        else
            $classified = new Classified();

        $form = $this->createForm(ClassifiedType::class, $classified);

        $form->handleRequest($request);

        if ($request->isMethod('POST')) {

          if ($form->isSubmitted() && $form->isValid()) {

              $classified->setOwner($user); // set the current user

              if($request->files->get('file')) {
                foreach($request->files->get('file') as $file){
                  $document = new Document();

                  $document->setUploadDir('classified_picture/');
                  $document->setFile($file);
                  $document->setName($file->getClientOriginalName());
                  $em->persist($document);

                  $classified->addPicture($document);

                }
              }

              $em->persist($classified);
              $em->flush();

              $this->addFlash(
                  'success',
                  'Ajouté avec succès'
              );

              return new JsonResponse(array('success' => 'Success', 'redirect' => $this->generateUrl('account_classified')), 200);
          }

          return new JsonResponse(array('error' => 'Error', 'message' => $form->getErrors()), 200);

        } else {

          return $this->render('AppBundle:Classified:edit.html.twig',array(
            'user' => $user,
            'classified' => $classified,
            'form' => $form->createView(),
          ));

        }

    }

    /**
     *
     * @Route("/account/reservations", name="account_reservation")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listReservationAction(Request $request)
    {
        return $this->render('AppBundle:User:listReservation.html.twig',array(
            'user' => $this->getUser(),
            'reservations' => $this->getUser()->getReservation(),
        ));
    }

    /**
     *
     * @Route("/account/conversations", name="account_conversation")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listConversationAction(Request $request)
    {
        return $this->render('AppBundle:User:listConversation.html.twig',array(
            'user' => $this->getUser()
        ));
    }

    /**
     *
     * @Route("account/approbation", name="account_approbation")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function approbationAction(Request $request)
    {
        if($request->isXmlHttpRequest()){

            $idReservation = $request->request->get('id');
            $status = $request->request->get('status');
            $em = $this->getDoctrine()->getEntityManager();
            $reservation = $em->getRepository("OdopData:Reservation")->find($idReservation);

            $reservation->setStatus($status);
            $em->persist($reservation);
            $em->flush();

            return new JsonResponse(array(
                'success' => 'true'
            ), 200);
        }

        return $this->render('AppBundle:User:show_message.html.twig',array(
            'user' => $this->getUser()
        ));
    }

    /**
     * Creates a new User entity.
     *
     * @Route("/register", name="user_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */

    public function createAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm('AppBundle\Form\RegistrationType', $user);

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {

                // encode the password
                $encoder = $this->container->get('security.password_encoder');
                $encoded = $encoder->encodePassword($user, $user->getPassword());
                $user->setPassword($encoded);

                $em = $this->getDoctrine()->getManager();

                // save the proposition
                $em->persist($user);
                $em->flush();

                return new JsonResponse(array(
                    'success' => 'Success',
                    'message' => 'Votre compte a bien été créé'),
                200);
            }
            else{
                return new JsonResponse(array(
                    'error' => false,
                    'errors' => $this->get('app.service.validation_form')->getErrorsAsArray($form)
                ));
            }
        }
        return $this->render('AppBundle:User:register.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to delete a Classified entity.
     *
     * @param Classified $classified The Classified entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Classified $classified)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('app_classified_delete', array('id' => $classified->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }
}
