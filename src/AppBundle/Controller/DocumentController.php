<?php

namespace AppBundle\Controller;

use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use OdopData\Entity\Document;
use OdopData\Repository\DocumentRepository;

/**
 * Classified controller.
 *
 * @Route("document")
 */
class DocumentController extends Controller
{

  /**
   * Delete a Document entity.
   *
   * @Route("/delete/{document}", requirements={"id" = "\d+"}, name="document_delete")
   * @Method("GET")
   * @param Document $document
   * @return \Symfony\Component\HttpFoundation\Response
   */
    public function deleteAction(Request $request, Document $document)
    {

        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($document);

        if($em->flush())
            return new JsonResponse(array('success' => 'Success'), 200);
        else
            return new JsonResponse(array('error' => 'Error', 'message' => 'Erreur lors de la suppression'), 200);

    }
}
