<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Ivory\GoogleMap\Place\AutocompleteComponentType;
use Ivory\GoogleMapBundle\Form\Type\PlaceAutocompleteType;

class QuickSearchType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('city', PlaceAutocompleteType::class, array(
              'types' => ['(cities)'],
              'components' => [AutocompleteComponentType::COUNTRY => 'fr'],
              'translation_domain' => 'messages'
            ))
            ->add('date_start', DateTimeType::class, array(
                'widget' => 'single_text',
                'html5' => false,
                'attr' => ['data-provider' => 'date_start'],
                'format' => 'dd-MM-yyyy HH:mm'))
            ->add('duree', IntegerType::class)
            ->add('maxParticipant')
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
        ));
    }
}
