<?php

namespace AppBundle\Controller;

use OdopData\Entity\Category;
use OdopData\Entity\Comment;
use OdopData\Entity\Parameter;
use OdopData\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\QuickSearchType;

/**
 * Homepage controller.
 */
class HomepageController extends Controller
{
    /**
     * Display Homepage.
     *
     * @Route("/", name="app_index")
     * @Method("GET")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {

        $quickSearch = $this->createForm('AppBundle\Form\QuickSearchType');

        $highlight = $this->getHighlight();
        $categorys = $this->getCategorys();

        return $this->render('AppBundle:Homepage:index.html.twig', array(
            'quickSearch' => $quickSearch->createView(),
            'highlight' => $highlight,
            'categorys' => $categorys,
        ));
    }

    /**
     * Display Homepage.
     *
     * @Route("/help", name="app_help")
     * @Method("GET")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function helpAction(Request $request)
    {


        return $this->render('AppBundle:Homepage:help.html.twig', array(
        ));
    }

    /**
     * Get Highlight.
     */
    private function getHighlight()
    {
        $em = $this->getDoctrine()->getManager();
        $highlights = $em->getRepository(Comment::class)->getPopularClassified();
        $parameters = $em->getRepository(Parameter::class)->getHighlight();

        foreach ($highlights as &$highlight){
            $highlight = $highlight[0]->getClassified();
        }

        return array(
            'title' => $parameters[0]->getValue(),
            'button' => $parameters[2]->getValue(),
            'items' => $highlights,
        );
    }

    /**
     * Get Categorys.
     */
    private function getCategorys()
    {
        $em = $this->getDoctrine()->getManager();
        $categorys = $em->getRepository(Category::class)->getPopularCategorys();


        foreach ($categorys as &$category){
            $category = $category[0];
        }

        return array(
            'items' => $categorys
        );
    }
}
