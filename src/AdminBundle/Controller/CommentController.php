<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use OdopData\Entity\Comment;
use AdminBundle\Form\CommentType;

/**
 * Comment controller.
 *
 * @Route("admin/comment")
 */
class CommentController extends Controller
{
    /**
     * Count Comment entities.
     */
    public function countAction()
    {
        $em = $this->getDoctrine()->getManager();
        $nb_comments = $em->getRepository(Comment::class)->countPending();

        return $this->render('AdminBundle:Partial:badge.html.twig', array(
            'nb' => $nb_comments,
        ));
    }
    /**
     * Lists all Comment entities.
     *
     * @Route("/{page}", name="admin_comment_index", defaults={"page": 1}, requirements={"page": "\d+"})
     * @Route("/{filter}/{page}", name="admin_comment_index_by", requirements={"filter": "pending|valide|deleted", "page": "\d+"}, defaults={"page": 1})
     * @Method("GET")
     * @param string $filter
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, $filter = "", $page)
    {
        $this->get('admin.breadcrumbs')->setBreadcrumbs(array("admin_comment_index" => "Commentaires"));
        $em = $this->getDoctrine()->getManager();

        if(!$filter)
            $comments = $em->getRepository(Comment::class)->findAll();
        else
            $comments = $em->getRepository(Comment::class)->findBy(array('status' => $filter));


        $paginator  = $this->get('knp_paginator');
        $result = $paginator->paginate(
            $comments, /* query NOT result */
            $request->query->getInt('page', $page)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('AdminBundle:Comment:index.html.twig', array(
            'comments' => $result,
        ));
    }

    /**
     * Creates a new Comment entity.
     *
     * @Route("/new", name="admin_comment_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $this->get('admin.breadcrumbs')->setBreadcrumbs(
            array("admin_comment_index" => "Commentaires",
                "admin_comment_new" => "Nouveau" ));

        $comment = new Comment();
        $form = $this->createForm('AdminBundle\Form\CommentType', $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();

            $this->addFlash(
                'success',
                'Ajouté avec succès'
            );

            return $this->redirectToRoute('admin_comment_show', array('id' => $comment->getId()));
        }

        return $this->render('AdminBundle:Comment:new.html.twig', array(
            'comment' => $comment,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Comment entity.
     *
     * @Route("/{id}/show", name="admin_comment_show")
     * @Method("GET")
     * @param Comment $comment
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Comment $comment)
    {
        $this->get('admin.breadcrumbs')->setBreadcrumbs(
            array("admin_comment_index" => "Commentaires",
                "admin_dashboard" => "Afficher" ));

        $deleteForm = $this->createDeleteForm($comment);
        $form = $this->createForm('AdminBundle\Form\CommentType', $comment);

        return $this->render('AdminBundle:Comment:show.html.twig', array(
            'comment' => $comment,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Comment entity.
     *
     * @Route("/{id}/edit", name="admin_comment_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Comment $comment
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Comment $comment)
    {
        $this->get('admin.breadcrumbs')->setBreadcrumbs(
            array("admin_comment_index" => "Commentaires",
                "admin_dashboard" => "Editer" ));

        $deleteForm = $this->createDeleteForm($comment);
        $form = $this->createForm('AdminBundle\Form\CommentType', $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();

            $this->addFlash(
                'success',
                'Modifications enregistrées avec succès'
            );

            return $this->redirectToRoute('admin_comment_edit', array('id' => $comment->getId()));
        }
        return $this->render('AdminBundle:Comment:edit.html.twig', array(
            'comment' => $comment,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Comment entity.
     *
     * @Route("/{id}", name="admin_comment_delete")
     * @Method("DELETE")
     * @param Request $request
     * @param Comment $comment
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Comment $comment)
    {
        $form = $this->createDeleteForm($comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($comment);
            $em->flush();

            $this->addFlash(
                'success',
                'Supprimé avec succès'
            );
        }

        return $this->redirectToRoute('admin_comment_index');
    }

    /**
     * Creates a form to delete a Comment entity.
     *
     * @param Comment $comment The Comment entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Comment $comment)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_comment_delete', array('id' => $comment->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     *
     * @Method({"GET", "POST"})
     * @Route("/ajax/snippet/comment/moderate", name="ajax_snippet_comment_moderate")
     * @param Request $request
     * @return JsonResponse
     */
    public function ajaxModerateCommentAction(Request $request)
    {
        $status = $request->request->get('target_status');
        $id = $request->request->get('target_id');
        $em = $this->getDoctrine()->getManager();

        $comment = $em->getRepository(Comment::class)->find($id);

        $comment->setStatus($status);
        $em->persist($comment);

        $em->flush();

        if($status == "deleted")
            $this->addFlash(
                'success',
                'Commentaire #' . $id . ' supprimé'
            );
        else {
            $this->addFlash(
                'success',
                'Commentaire #' . $id . ' validé'
            );
        }

        return new JsonResponse(array('success' => true));
    }

}
