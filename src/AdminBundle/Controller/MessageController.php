<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use OdopData\Entity\Message;
use AdminBundle\Form\MessageType;

/**
 * Message controller.
 *
 * @Route("admin/message")
 */
class MessageController extends Controller
{
    /**
     * Lists all Message entities.
     *
     * @Route("/{page}", name="admin_message_index", defaults={"page": 1})
     * @Method("GET")
     */
    public function indexAction(Request $request, $page)
    {
        $this->get('admin.breadcrumbs')->setBreadcrumbs(array("admin_message_index" => "Messages"));
        $em = $this->getDoctrine()->getManager();

        $messages = $em->getRepository('AdminBundle:Message')->findAll();

        $paginator  = $this->get('knp_paginator');
        $result = $paginator->paginate(
            $messages, /* query NOT result */
            $request->query->getInt('page', $page)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('message/index.html.twig', array(
            'messages' => $result,
        ));
    }

    /**
     * Creates a new Message entity.
     *
     * @Route("/new", name="admin_message_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $this->get('admin.breadcrumbs')->setBreadcrumbs(
            array("admin_message_index" => "Messages",
                "admin_message_new" => "Nouveau" ));

        $message = new Message();
        $form = $this->createForm('AdminBundle\Form\MessageType', $message);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($message);
            $em->flush();

            $this->addFlash(
                'success',
                'Ajouté avec succès'
            );

            return $this->redirectToRoute('message_show', array('id' => $message->getId()));
        }

        return $this->render('message/new.html.twig', array(
            'message' => $message,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Message entity.
     *
     * @Route("/{id}/show", name="admin_message_show")
     * @Method("GET")
     * @param Message $message
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Message $message)
    {
        $this->get('admin.breadcrumbs')->setBreadcrumbs(
            array("admin_message_index" => "Messages",
                "admin_dashboard" => "Afficher" ));

        $deleteForm = $this->createDeleteForm($message);
        $form = $this->createForm('AdminBundle\Form\MessageType', $message);

        return $this->render('message/show.html.twig', array(
            'message' => $message,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Message entity.
     *
     * @Route("/{id}/edit", name="admin_message_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Message $message
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Message $message)
    {
        $this->get('admin.breadcrumbs')->setBreadcrumbs(
            array("admin_message_index" => "Categories",
                "admin_dashboard" => "Editer" ));

        $deleteForm = $this->createDeleteForm($message);
        $form = $this->createForm('AdminBundle\Form\MessageType', $message);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($message);
            $em->flush();

            $this->addFlash(
                'success',
                'Modifications enregistrées avec succès'
            );

            return $this->redirectToRoute('message_edit', array('id' => $message->getId()));
        }

        return $this->render('message/edit.html.twig', array(
            'message' => $message,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Message entity.
     *
     * @Route("/{id}", name="admin_message_delete")
     * @Method("DELETE")
     * @param Request $request
     * @param Message $message
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Message $message)
    {
        $form = $this->createDeleteForm($message);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($message);
            $em->flush();

            $this->addFlash(
                'success',
                'Supprimé avec succès'
            );
        }

        return $this->redirectToRoute('message_index');
    }

    /**
     * Creates a form to delete a Message entity.
     *
     * @param Message $message The Message entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Message $message)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('message_delete', array('id' => $message->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
