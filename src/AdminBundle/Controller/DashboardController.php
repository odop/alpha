<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use OdopData\Entity\Classified;
use OdopData\Entity\Reservation;
use OdopData\Entity\User;
use OdopData\Entity\Comment;

/**
 * Dashboard controller.
 *
 * @Route("admin")
 */
class DashboardController extends Controller
{
    /**
     * Lists all User entities.
     *
     * @Route("/", name="admin_dashboard")
     * @Method("GET")
     */
    public function indexAction()
    {
        $this->get('admin.breadcrumbs')->setBreadcrumbs(array("admin_dashboard" => "Accueil"));
        $em = $this->getDoctrine()->getManager();

        $classifieds = $em->getRepository(Classified::class)->findAll();
        $reservations = $em->getRepository(Reservation::class)->findAll();
        $users = $em->getRepository(User::class)->findAll();
        $comments = $em->getRepository(Comment::class)->findAll();

        return $this->render('AdminBundle:Dashboard:index.html.twig', array(
            'classifieds' => $classifieds,
            'reservations' => $reservations,
            'users' => $users,
            'comments' => $comments,
        ));
    }
}
