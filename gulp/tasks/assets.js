var gulp  = require('gulp');
var paths = require('../config');

// copy assets
gulp.task('assets', function() {
  gulp.src(paths.assets.all)
    .pipe(gulp.dest(paths.assets.to));
});
