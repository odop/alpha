<?php

namespace AdminBundle\Service;

class highlight
{

    protected $_higtlight;

    public function setBreadcrumbs ($items)
    {

        $this->_breadcrumbs->addRouteItem("Administration", "admin_dashboard");

        $numItems = count($items);
        $i = 0;
        foreach ($items as $route => $item) {
            if( ++$i === $numItems ){
                $this->_breadcrumbs->addItem($item);
            }
            else{
                $this->_breadcrumbs->addRouteItem($item, $route);
            }
        }
    }
}