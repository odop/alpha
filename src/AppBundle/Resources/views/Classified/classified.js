
export default function classified() {

    let form = $("#add_classified");

    window.myDropzone = new Dropzone("#add_classified", { // The camelized version of the ID of the form element

      // The configuration we've talked about above
      paramName: 'file',
      clickable:'.dz-upload',
      previewsContainer:'.dz-upload',
      autoProcessQueue: false,
      uploadMultiple: true,
      addRemoveLinks: true,
      dictRemoveFileConfirmation: 'Supprimer cette image ?',
      parallelUploads: 100,
      maxFiles: 100,

      // The setting up of the dropzone
      init: function() {

        var myDropzone = this;

        // First change the button to actually tell Dropzone to process the queue.
        this.element.querySelector("input[type=submit]").addEventListener("click", function(e) {
          // Make sure that the form isn't actually being sent.
          if(form[0].checkValidity()) {
              e.preventDefault();
              e.stopPropagation();
              myDropzone.processQueue();
          }
        });

        this.on("addedfile", function(file) {
          $(".dz-preview:last-child").attr('data-id', file.id);

          $(".dz-message").hide();
        }),

        this.on("removedfile", function(file) {

          $.ajax({
              type: 'get',
              url: file.delUrl,
              success: function(response) {
                  console.log(response);
              }
          });
        }),

        this.on("removededfile", function() {
            // $(".dz-message").show();
        }),

        // Listen to the sendingmultiple event. In this case, it's the sendingmultiple event instead
        // of the sending event because uploadMultiple is set to true.
        this.on("sendingmultiple", function(file, xhr, formData, e) {
          // Gets triggered when the form is actually being sent.
          // Hide the success button or the complete form.

          $.each( form.serializeArray(), function( index, value ){
              formData.append(value['name'], value['value']);
          });

        });
        this.on("successmultiple", function(files, response) {
          // Gets triggered when the files have successfully been sent.
          // Redirect user or notify of success.
          setTimeout(function(){
            window.location.href = response.redirect;
          }, 2000);
        });
        this.on("errormultiple", function(files, response) {
          // Gets triggered when there was an error sending the files.
          // Maybe show form again, and notify user of error
          // console.log(response);
        });

      },

    });


}

classified.selector = '#add_classified';
