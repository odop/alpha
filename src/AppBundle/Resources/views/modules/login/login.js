export default function login() {

      $(document).ready(function () {

          let login_form = $('#login_form');
          let facebook_form = $('#facebook-form');
          let google_form = $('#google-form');

          $("#login-trigger").click(function(){
              $('#signinModal').modal('hide');
              $('#loginModal').modal('show');
          });

          $('#loginModal .login-submit').on("click", function (e) {
              e.preventDefault();
              $('#loginModal .login-submit').html('<i class="fa fa-spinner fa-spin"></i>');

              $.ajax({
                  type: login_form.attr('method'),
                  url: login_form.attr('action'),
                  data: login_form.serialize(),
                  success: function(response) {
                      if(response.success) {
                          location.reload(true);
                      } else {
                          if(response.message === "Bad credentials.")
                            response.message = "Adresse email ou mot de passe incorrect."
                          $('#loginModal .login-submit').html('Connexion');
                          $('#login-alert .alert-danger').removeClass("hidden");
                          $('#login-alert .alert-danger .alert-message').html(response.message);
                      }
                  }
              });
          });

          /*$('#loginModal .login-social').on("click", function (e) {

              let social_media = $(this).attr('data-id');
              let social_form = social_media === 'facebook' ? facebook_form : google_form;
              console.log(social_form);
              console.log(social_form.attr('action'));
              e.preventDefault();
              $.ajax({
                  type: social_form.attr('method'),
                  url: social_form.attr('action'),
                  data: social_form.serialize(),
                  success: function(response) {
                      if(response.success) {
                          console.log("ALLLLLOOOO");
                          location.reload(true);
                      }
                  }
              });
          });
        */
      });
}

login.selector = '#loginModal';
