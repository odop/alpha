<?php

namespace AppBundle\Service;

/**
 * Class ValidationForm
 * Service permettant de gérer les validation des formulaires
 * @package AppBundle\Service
 */
class ValidationForm
{
    /**
     * @param $form
     * @return array
     */
    public function getErrorsAsArray($form){
        $errors = array();
        foreach ($form->getErrors() as $error)
            $errors[] = $error->getMessage();

        foreach ($form->all() as $key => $child) {
            if ($err = $this->getErrorsAsArray($child))
                $errors[$key] = $err;
        }
        return $errors;
    }
}