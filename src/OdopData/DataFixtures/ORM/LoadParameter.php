<?php

namespace OdopData\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use OdopData\Entity\Parameter;

class LoadParameter extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // Liste des noms de catégorie à ajouter
        $parameters = array(
            array('name' => 'Titre du site', 'value' => 'ODOP', 'category' => $this->getReference('Site_params_0')),
            array('name' => 'Url du site', 'value' => 'http://local.odop.fr/app_dev.php', 'category' => $this->getReference('Site_params_0')),
            array('name' => 'Email admin', 'value' => 'admin@odop.fr', 'category' => $this->getReference('Site_params_0')),
            array('name' => 'Titre', 'value' => 'Un jardin pour cet été', 'category' => $this->getReference('Site_params_1')),
            array('name' => 'Catégorie', 'value' => $this->getReference('Type_space_1')->getId() , 'category' => $this->getReference('Site_params_1')),
            array('name' => 'Bouton', 'value' => 'Découvrir d\'autre jardin', 'category' => $this->getReference('Site_params_1')),
        );

        foreach ($parameters as $k => $param) {
            $parameter = new Parameter();
            $parameter->setName($param['name']);
            $parameter->setValue($param['value']);
            $parameter->setCategory($param['category']);

            // On la persiste
            $manager->persist($parameter);

            $this->addReference($param['value'].$k, $parameter);
        }

        // On déclenche l'enregistrement de toutes les catégories
        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 8;
    }
}
