<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use OdopData\Entity\Category;
use AdminBundle\Form\CategoryType;

/**
 * Category controller.
 *
 * @Route("admin/category")
 */
class CategoryController extends Controller
{

    /**
     * Lists all Category entities.
     *
     * @Route("/{page}", name="admin_category_index", defaults={"page": 1})
     * @Method("GET")
     */
    public function indexAction(Request $request, $page)
    {
        $this->get('admin.breadcrumbs')->setBreadcrumbs(array("admin_category_index" => "Catégories"));

        $em = $this->getDoctrine()->getManager();

        $categorys = $em->getRepository(Category::class)->findAll();

        $paginator  = $this->get('knp_paginator');
        $result = $paginator->paginate(
            $categorys, /* query NOT result */
            $request->query->getInt('page', $page)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('AdminBundle:Category:index.html.twig', array(
            'categorys' => $result,
        ));
    }

    /**
     * Creates a new Category entity.
     *
     * @Route("/new", name="admin_category_new")
     * @param Request $request
     * @return Object
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $this->get('admin.breadcrumbs')->setBreadcrumbs(
            array("admin_category_index" => "Catégories",
                "admin_category_new" => "New" ));

        $category = new Category();
        $form = $this->createForm('AdminBundle\Form\CategoryType', $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            $this->addFlash(
                'success',
                'Ajouté avec succès'
            );

            return $this->redirectToRoute('admin_category_show', array('id' => $category->getId()));
        }

        return $this->render('AdminBundle:Category:new.html.twig', array(
            'category' => $category,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Category entity.
     *
     * @Route("/{id}/show", name="admin_category_show")
     * @Method("GET")
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Category $category)
    {
        $this->get('admin.breadcrumbs')->setBreadcrumbs(
            array("admin_category_index" => "Catégories",
                "admin_dashboard" => "Afficher" ));

        $deleteForm = $this->createDeleteForm($category);
        $form = $this->createForm('AdminBundle\Form\CategoryType', $category);

        return $this->render('AdminBundle:Category:show.html.twig', array(
            'category' => $category,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Category entity.
     *
     * @Route("{id}/edit/", name="admin_category_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Category $category)
    {
        $this->get('admin.breadcrumbs')->setBreadcrumbs(
            array("admin_category_index" => "Catégories",
                "admin_dashboard" => "Editer" ));

        $deleteForm = $this->createDeleteForm($category);
        $form = $this->createForm('AdminBundle\Form\CategoryType', $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            $this->addFlash(
                'success',
                'Modifications enregistrées avec succès'
            );

            return $this->redirectToRoute('admin_category_edit', array('id' => $category->getId()));
        }

        return $this->render('AdminBundle:Category:edit.html.twig', array(
            'category' => $category,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Category entity.
     *
     * @Route("/{id}", name="admin_category_delete")
     * @Method("DELETE")
     * @param Request $request
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Category $category)
    {
        $form = $this->createDeleteForm($category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($category);
            $em->flush();

            $this->addFlash(
                'success',
                'Supprimé avec succès'
            );
        }

        return $this->redirectToRoute('admin_category_index');
    }

    /**
     * Creates a form to delete a Category entity.
     *
     * @param Category $category The Category entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Category $category)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_category_delete', array('id' => $category->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
