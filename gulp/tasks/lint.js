var gulp   = require('gulp');
var paths  = require('../config');
var eslint = require('gulp-eslint');

// lint
gulp.task('lint', function() {
  return gulp.src([paths.scripts.all, paths.node.all])
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failOnError());
});
