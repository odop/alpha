<?php

namespace AdminBundle\Service;

class Breadcrumbs
{

    protected $_breadcrumbs;

    /**
     * Breadcrumbs constructor.
     * @param \WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs $breadcrumbs
     */
    public function __construct(\WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs $breadcrumbs)
    {
        $this->_breadcrumbs = $breadcrumbs;
    }

    /**
     * @param $items
     */
    public function setBreadcrumbs ($items)
    {

        $this->_breadcrumbs->addRouteItem("Administration", "admin_dashboard");

        $numItems = count($items);
        $i = 0;
        foreach ($items as $route => $item) {
            if( ++$i === $numItems ){
                $this->_breadcrumbs->addItem($item);
            }
            else{
                $this->_breadcrumbs->addRouteItem($item, $route);
            }
        }
    }
}