<?php

namespace AppBundle\Controller;

use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use OdopData\Entity\Conversation;
use OdopData\Entity\Message;
use OdopData\Entity\Reservation;
use OdopData\Entity\Classified;
use AppBundle\Form\ReservationType;
use AppBundle\Form\MessageType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ReservationController
 * @package AppBundle\Controller
 * @Route("reservation")
 */

class ReservationController extends Controller
{
    public function indexAction()
    {
        return $this->render('AppBundle:Booking:index.html.twig');
    }

    /**
     * @Method({"GET"})
     * @Route("/show", name="show_reservation")
     * @param Request $request
     * @param Classified $classified
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
      public function showAction(Request $request,Classified $classified = null)
      {
        $em = $this->getDoctrine()->getManager();

        $reservation = new Reservation();

        if (!$classified) {
          throw $this->createNotFoundException('Unable to find classified entity.');
        }

        $reservations = $em->getRepository('OdopData:Reservation')->getClassifiedByReservation($classified->getId());

        $reservation->setClassified($classified);

        $form = $this->createForm(ReservationType::class, $reservation);

        $form->handleRequest($request);

        return $this->render('AppBundle:Reservation:show.html.twig', array(
            'classified' => $classified,
            'reservations' => $reservations,
            'form' => $form->createView(),
        ));
      }

    /**
     * @Route("/new", name="app_reservation_new")
     * @Method("POST")
     * @param Request $request
     * @param Classified $classified
     * @return JsonResponse
     */
    public function newAction(Request $request, Classified $classified = null){

        $reservation = new Reservation();

        $em = $this->getDoctrine()->getManager();

        if($request->isMethod('POST')){


            $form = $this->createForm(ReservationType::class, $reservation);
            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()) {

                $conversation = new Conversation();
                $message = new Message();

                $data = $request->request->all();
                $user = $this->get('security.token_storage')->getToken()->getUser();

                // @Todo Création d'un service pour la gestion des dates et les calculs
                // On récupère les dates dans un format permettant de les enregistrer en base
                $dateStart = \DateTime::createFromFormat('d/m/Y H:i', $data['reservation']['dateStart']);
                $dateEnd   = \DateTime::createFromFormat('d/m/Y H:i', $data['reservation']['dateEnd']);

                // On transforme en timestamp les dates pour calculer le temps entre les deux
                $timestamp1 = strtotime($dateStart->format('Y-m-d H:i:s'));
                $timestamp2 = strtotime($dateEnd->format('Y-m-d H:i:s'));

                $time = abs($timestamp2 - $timestamp1)/3600;

                $reservation->setPrice($time * $reservation->getClassified()->getPrice());
                $reservation->setUser($user);

                $em->persist($reservation);
                $em->flush();

                // On enregistre la conversation lié à la réservation
                $conversation->setReservation($reservation);
                $conversation->addUser($reservation->getClassified()->getOwner());
                $conversation->addUser($user);
                $em->persist($conversation);
                $em->flush();

                // On enregistre le message lié à la conversation nouvellement créé
                $message->setConversation($conversation);
                $message->setContent($data['message']['content']);
                $message->setAuthor($user);
                $em->persist($message);
                $em->flush();


                // dump($reservation);
                // dump($conversation);
                // dump($message);
                // die();

                return new JsonResponse(array('success' => 'Success', 'message' => 'Votre réservation est maintenant en cours d\'approbation'), 200);

            }

            return new JsonResponse(array('error' => 'Error', 'message' => $form->getErrors()), 200);
        }

        if (!$classified) {
          throw $this->createNotFoundException('Unable to find classified entity.');
        }

        $reservation->setClassified($classified);

        $form = $this->createForm(ReservationType::class, $reservation);
        $form->handleRequest($request);

        return $this->render('AppBundle:Reservation:modal.html.twig', array(
          'reservation' => $reservation,
          'classified' => $classified,
          'form' => $form->createView(),
        ));

    }


    /**
     * @Route("/edit/{id}", name="app_reservation_edit")
     * @Method("GET")
     * @param Request $request
     * @param $id
     */

    public function editAction(Request $request, $id){

    }

    /**
     * @Route("/delete/{id}", name="app_reservation_delete")
     * @Method("GET")
     * @param Request $request
     * @param $id
     */

    public function deleteAction(Request $request, $id){

    }

    /**
     * @Route("/recap", name="app_reservation_recap")
     * @Method({"GET","POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function recapAction(Request $request){

        $idReservation = $request->request->get('id');
        $isPayed = $request->request->get('isPayed');
        $em = $this->getDoctrine()->getEntityManager();
        $reservation = $em->getRepository("OdopData:Reservation")->find($idReservation);

        if(!$reservation) {
            throw $this->createNotFoundException('Unable to find the reservation');
        }

        if($isPayed) {
            $reservation->setIsPayed(1);
            $em->persist($reservation);
            $em->flush();

            return new JsonResponse(array('success' => 'Success'), 200);
        }

        $reservationObject = array(
            'dateStart' => $reservation->getDateStart()->format('d/m/Y H:i:s'),
            'dateEnd' => $reservation->getDateEnd()->format('d/m/Y H:i:s'),
            'price' => $reservation->getPrice(),
            'participant' => $reservation->getParticipant(),
        );

        return new JsonResponse(array('success' => 'Success', 'reservation' => $reservationObject), 200);
    }
}
