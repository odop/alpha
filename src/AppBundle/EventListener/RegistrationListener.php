<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\EventListener;

use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\FOSUserEvents;
use AppBundle\Service\ValidationForm;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class RegistrationListener implements EventSubscriberInterface
{
    private $requestStack;
    private $validationForm;

    /**
     * EmailConfirmationListener constructor.
     *
     * @param RequestStack         $requestStack
     * @param ValidationForm       $validationForm
     */
    public function __construct(RequestStack $requestStack, ValidationForm $validationForm)
    {
        $this->requestStack = $requestStack;
        $this->validationForm = $validationForm;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            FOSUserEvents::REGISTRATION_SUCCESS => [
                ['onRegistrationSuccess', -10],
            ],
            FOSUserEvents::REGISTRATION_FAILURE => [
                'onRegistrationFailure'
            ]
        ];
    }

    /**
     * @param FormEvent $event
     */
    public function onRegistrationSuccess(FormEvent $event)
    {
        $request = $this->requestStack->getCurrentRequest();

        if( $request->isXmlHttpRequest() ){
            $response = new JsonResponse(
                array(
                    'success' => true,
                    'message' => "Un email vous a été envoyé pour confirmer l'inscription"
                ),
                200);

            return $event->setResponse($response);
        }

        $response = new JsonResponse(
            array(
                'success' => false,
                'message' => "Une erreur est survenu ..."
            ),
        400);
        $event->setResponse($response);
    }

    public function onRegistrationFailure(FormEvent $event) {

        $form = $event->getForm();

        $validationErrors = $this->validationForm->getErrorsAsArray($form);

        $response = new JsonResponse(
            array(
                'success' => false,
                'errors' => $validationErrors
            ),
            200);

        $event->setResponse($response);
    }
}
