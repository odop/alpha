<?php

namespace OdopData\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use OdopData\Entity\Comment;

class LoadComment extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // Liste des noms de catégorie à ajouter
        $comments = array(
            array('content' => 'Locataire très sympa, je recommande',
                'rate' => 5,
                'user' => $this->getReference('User_1'),
                'classified' => $this->getReference('Classified_1'),
                'status' => 'valide'
            ),
            array('content' => 'La location était agrable',
                'rate' => 3,
                'user' => $this->getReference('User_0'),
                'classified' => $this->getReference('Classified_0'),
                'status' => 'valide'
            ),
            array('content' => 'Les locataires sont très gentils et sont serviable',
                'rate' => 4,
                'user' => $this->getReference('User_0'),
                'classified' => $this->getReference('Classified_3'),
                'status' => 'valide'
            ),
            array('content' => 'Merci pour votre gentillesse',
                'rate' => 4,
                'user' => $this->getReference('User_1'),
                'classified' => $this->getReference('Classified_2'),
                'status' => 'valide'
            ),
            array('content' => 'Un agréable moment, merci.',
                'rate' => 5,
                'user' => $this->getReference('User_1'),
                'classified' => $this->getReference('Classified_4'),
                'status' => 'valide'
            ),
            array('content' => 'Les propriétaires sont à l\'écoute pour notre plus grand bonheur',
                'rate' => 3,
                'user' => $this->getReference('User_0'),
                'classified' => $this->getReference('Classified_3'),
                'status' => 'deleted'
            ),
            array('content' => 'Super, rien à dire.',
                'rate' => 2,
                'user' => $this->getReference('User_0'),
                'classified' => $this->getReference('Classified_0'),
                'status' => 'pending'
            ),
            array('content' => 'Au top, merci odop !',
                'rate' => 1,
                'user' => $this->getReference('User_0'),
                'classified' => $this->getReference('Classified_0'),
                'status' => 'valide'
            ),
            array('content' => 'C\'était vraiment trop bien !',
                'rate' => 4,
                'user' => $this->getReference('User_1'),
                'classified' => $this->getReference('Classified_0'),
                'status' => 'valide'
            ),
            array('content' => 'Tous les invités étaient ravis, merci odop.',
                'rate' => 5,
                'user' => $this->getReference('User_1'),
                'classified' => $this->getReference('Classified_3'),
                'status' => 'deleted'
            ),
            array('content' => 'Enfin un site à la hauteur !!!',
                'rate' => 4,
                'user' => $this->getReference('User_0'),
                'classified' => $this->getReference('Classified_2'),
                'status' => 'valide'
            ),
            array('content' => 'Service réussi et une réponse très rapide des propriétaires',
                'rate' => 4,
                'user' => $this->getReference('User_1'),
                'classified' => $this->getReference('Classified_2'),
                'status' => 'valide'
            ),
            array('content' => 'Merci aux propriétaires',
                'rate' => 3,
                'user' => $this->getReference('User_0'),
                'classified' => $this->getReference('Classified_4'),
                'status' => 'pending'
            ),
        );

        foreach ($comments as $com) {
            $comment = new Comment();
            $comment->setContent($com['content']);
            $comment->setRate($com['rate']);
            $comment->setUser($com['user']);
            $comment->setClassified($com['classified']);
            $comment->setStatus($com['status']);

            // On la persiste
            $manager->persist($comment);
        }

        // On déclenche l'enregistrement de toutes les catégories
        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 7;
    }
}
