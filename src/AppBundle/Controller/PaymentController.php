<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PaymentController extends Controller
{
    public function indexAction()
    {
        return $this->render('AppBundle:Payment:index.html.twig');
    }
}
