<?php

namespace AppBundle\Controller;

use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use OdopData\Entity\Classified;
use OdopData\Entity\Reservation;
use AdminBundle\Form\ClassifiedType;
use AppBundle\Form\QuickSearchType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Constraints\DateTime;
use Ivory\GoogleMap\Map;
use Ivory\GoogleMap\Base\Coordinate;
use Ivory\GoogleMap\Overlay\Marker;
use Ivory\GoogleMap\MapTypeId;

/**
 * Classified controller.
 *
 * @Route("classified")
 */
class ClassifiedController extends Controller
{
    /**
     * Lists all Classified entities.
     *
     * @Route("/", name="app_classified_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        return $this->render('AppBundle:Advert:index.html.twig');
    }

    /**
     * Finds and displays a Classified entity.
     *
     * @Route("/{id}", requirements={"id" = "\d+"}, name="app_classified_show")
     * @Method("GET")
     * @param Classified $classified
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Classified $classified)
    {

        $datesReservation = [];
        $em = $this->getDoctrine();
        $reservations = $em->getRepository('OdopData:Reservation')->getClassifiedByReservation($classified->getId());
        $classified = $em->getRepository('OdopData:Classified')->getOneClassified($classified->getId());

        // if(null === $reservations){
        //     return $this->render('AppBundle:Classified:show.html.twig', array(
        //         'classified' => $classified
        //     ));
        // }

        // Get list of dates already reserved for this classified
        foreach($reservations as $key => $value){
            $datesReservation[] = $value['dateStart']->format('Y-m-d H:i:s');
            $datesReservation[] = $value['dateEnd']->format('Y-m-d H:i:s');
        }

        return $this->render('AppBundle:Classified:show.html.twig', array(
            'classified' => $classified,
            'reservations' => $datesReservation
        ));
    }

    /**
     * Search and displays a Classified entity.
     *
     * @Route("/search/{page}", name="app_classified_search_all",
     *     defaults={"page": 1}, requirements={"page": "\d+"})
     * @Route("/search/{category}/{page}", name="app_classified_search",
     *     defaults={"page": 1}, requirements={"page": "\d+"})
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param $category
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function searchAction(Request $request, $category=null, $page)
    {
        $em = $this->getDoctrine()->getManager();

        $quickSearch = $this->createForm('AppBundle\Form\QuickSearchType');
        $quickSearch->handleRequest($request);

        $map = new Map();

        $map->setHtmlId('map');

        $map->setAutoZoom(true);

        if ($quickSearch->isValid()) {

            $data = $quickSearch->getData();

            if($data['duree']){
              $data['date_end'] = clone $data['date_start'];
              $data['date_end']->modify('+'. $data['duree'] .' hour');
              unset($data['duree']);
            }
            $classifieds = $em->getRepository(Classified::class)->getClassifiedsByFilter($data);

        } else if( $category !== null ){
            $classifieds = $em->getRepository(Classified::class)->getClassifieds(array("category" => $category));
        } else {
            $classifieds = $em->getRepository(Classified::class)->getClassifiedsByFilter();
        }

        $paginator  = $this->get('knp_paginator');
        $result = $paginator->paginate(
            $classifieds, /* query NOT result */
            $request->query->getInt('page', $page)/*page number*/,
            4/*limit per page*/
        );

        foreach ($result as $k) {
            if( !$k->getLocationLat() && !$k->getLocationLng() ) {
                $k->setLocationLat('48.8534');
                $k->setLocationLng('2.3488');
            }
            $marker = new Marker(new Coordinate($k->getLocationLat(), $k->getLocationLng()));
            $map->getOverlayManager()->addMarker($marker);
        }

        $map->setMapOption('styles' , [
            [
                "featureType"=> "administrative.neighborhood",
                "elementType"=> "all",
                "stylers"=> [
                    ["visibility"=> "on"]
                ]
            ],
            [
                "featureType"=> "administrative.neighborhood",
                "elementType"=> "labels",
                "stylers"=> [
                    ["visibility"=> "off"]
                ]
            ],
            [
                "featureType"=> "administrative.land_parcel",
                "elementType"=> "geometry.stroke",
                "stylers"=> [
                    ["visibility"=> "off"]
                ]
            ],
            [
                "featureType"=> "landscape.man_made",
                "elementType"=> "all",
                "stylers"=> [
                    ["visibility"=> "off"]
                ]
            ],
            [
                "featureType"=> "landscape.natural",
                "elementType"=> "geometry.fill",
                "stylers"=> [
                    ["color"=> "#f5f6f5"]
                ]
            ],
            [
                "featureType"=> "landscape.natural.terrain",
                "elementType"=> "all",
                "stylers"=> [
                    ["visibility"=> "off"]
                ]
            ],
            [
                "featureType"=> "poi",
                "elementType"=> "labels",
                "stylers"=> [
                    ["visibility"=> "off"]
                ]
            ],
            [
                "featureType"=> "poi.attraction",
                "elementType"=> "geometry.fill",
                "stylers"=> [
                    ["visibility"=> "off"]
                ]
            ],
            [
                "featureType"=> "poi.business",
                "elementType"=> "geometry.fill",
                "stylers"=> [
                    ["visibility"=> "off"]
                ]
            ],
            [
                "featureType"=> "poi.government",
                "elementType"=> "geometry.fill",
                "stylers"=> [
                    ["visibility"=> "off"]
                ]
            ],
            [
                "featureType"=> "poi.medical",
                "elementType"=> "geometry.fill",
                "stylers"=> [
                    ["visibility"=> "off"]
                ]
            ],
            [
                "featureType"=> "poi.park",
                "elementType"=> "all",
                "stylers"=> [
                    ["color"=> "#00bfa0"]
                ]
            ],
            [
                "featureType"=> "poi.place_of_worship",
                "elementType"=> "geometry.fill",
                "stylers"=> [
                    ["visibility"=> "off"]
                ]
            ],
            [
                "featureType"=> "poi.school",
                "elementType"=> "geometry.fill",
                "stylers"=> [
                    ["visibility"=> "off"]
                ]
            ],
            [
                "featureType"=> "poi.sports_complex",
                "elementType"=> "geometry.fill",
                "stylers"=> [
                    ["visibility"=> "off"]
                ]
            ],
            [
                "featureType"=> "road",
                "elementType"=> "labels",
                "stylers"=> [
                    ["visibility"=> "off"]
                ]
            ],
            [
                "featureType"=> "road.highway",
                "elementType"=> "geometry.fill",
                "stylers"=> [
                    ["saturation"=> "16"],
                    ["lightness"=> "25"]
                ]
            ],
            [
                "featureType"=> "road.highway",
                "elementType"=> "geometry.stroke",
                "stylers"=> [
                    ["lightness"=> "100"],
                    ["visibility"=> "off"]
                ]
            ],
            [
                "featureType"=> "road.highway",
                "elementType"=> "labels.icon",
                "stylers"=> [
                    ["saturation"=> "0"],
                    ["lightness"=> "29"]
                ]
            ],
            [
                "featureType"=> "road.highway.controlled_access",
                "elementType"=> "geometry.fill",
                "stylers"=> [
                    ["color"=> "#ffb400"]
                ]
            ],
            [
                "featureType"=> "road.arterial",
                "elementType"=> "geometry.fill",
                "stylers"=> [
                    ["visibility"=> "on"]
                ]
            ],
            [
                "featureType"=> "road.arterial",
                "elementType"=> "geometry.stroke",
                "stylers"=> [
                    ["visibility"=> "off"]
                ]
            ],
            [
                "featureType"=> "road.arterial",
                "elementType"=> "labels.icon",
                "stylers"=> [
                    ["visibility"=> "off"]
                ]
            ],
            [
                "featureType"=> "road.local",
                "elementType"=> "geometry.stroke",
                "stylers"=> [
                    ["visibility"=> "off"]
                ]
            ],
            [
                "featureType"=> "road.local",
                "elementType"=> "labels.icon",
                "stylers"=> [
                    ["visibility"=> "off"]
                ]
            ],
            [
                "featureType"=> "transit",
                "elementType"=> "all",
                "stylers"=> [
                    ["visibility"=> "off"]
                ]
            ],
            [
                "featureType"=> "transit.station.airport",
                "elementType"=> "geometry.fill",
                "stylers"=> [
                    ["hue"=> "#a000ff"],
                    ["saturation"=> "74"]
                ]
            ],
            [
                "featureType"=> "water",
                "elementType"=> "geometry.fill",
                "stylers"=> [
                    ["color"=> "#007a87"]
                ]
            ]
        ]);

        return $this->render('AppBundle:Classified:search.html.twig', array(
            'classifieds' => $result,
            'quickSearch' => $quickSearch->createView(),
            'map'         => $map,
        ));
    }

    /**
     *
     * @Route("/edit/{id}", name="app_classified_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Classified $classified
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Classified $classified)
    {

        $deleteForm = $this->createDeleteForm($classified);
        $form = $this->createForm('AppBundle\Form\ClassifiedType', $classified);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($classified);
            $em->flush();

            $this->addFlash(
                'success',
                'Modifications enregistrées avec succès'
            );

            return $this->redirectToRoute('app_classified_edit', array('id' => $classified->getId()));
        }

        return $this->render('AppBundle:Classified:edit.html.twig',array(
            'classified' => $classified,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView()
        ));
    }

    /**
     *
     * @Route("/delete/{id}", name="app_classified_delete")
     * @Method("DELETE")
     * @param Request $request
     * @param Classified $classified
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction(Request $request, Classified $classified)
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $classifiedResult = $em->getRepository('OdopData:Classified')->find($classified->getId());

        if( null === $classifiedResult ){
            throw new NotFoundHttpException('L\'annonce n° ' . $classifiedResult->getId() . ' n\'a pas été trouvé');
        }

        $form = $this->createDeleteForm($classifiedResult);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->remove($classifiedResult);
            $em->flush();

            $this->addFlash(
                'success',
                'Supprimé avec succès'
            );
        }

        return $this->redirectToRoute('account_classified', array(
            'id' => $user->getId()
        ));
    }

    /**
     * Creates a form to delete a Classified entity.
     *
     * @param Classified $classified The Classified entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Classified $classified)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('app_classified_delete', array('id' => $classified->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }

}
