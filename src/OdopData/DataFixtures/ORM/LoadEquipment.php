<?php

namespace OdopData\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use OdopData\Entity\Equipment;

class LoadEquipment extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // Liste des noms de catégorie à ajouter
        $equipments = array(
            array('name' => 'Wifi', 'picto_url' => 'fa fa-signal', 'category' => $this->getReference('Type_event_0')),
            array('name' => 'Sound system', 'picto_url' => 'fa fa-volume-up', 'category' => $this->getReference('Type_event_0')),
            array('name' => 'Vestiaire', 'picto_url' => 'fa fa-tag', 'category' => $this->getReference('Type_event_0')),
            array('name' => 'Cuisine', 'picto_url' => 'fa fa-fire', 'category' => $this->getReference('Type_event_0')),
            array('name' => 'Couverts', 'picto_url' => 'fa fa-cutlery', 'category' => $this->getReference('Type_event_0')),
            array('name' => 'Couchage', 'picto_url' => 'fa fa-bed', 'category' => $this->getReference('Type_event_0')),
        );

        foreach ($equipments as $k => $equip) {
            $equipment = new Equipment();
            $equipment->setName($equip['name']);
            $equipment->setPictoUrl($equip['picto_url']);
            $equipment->setCategory($equip['category']);

            // On la persiste
            $manager->persist($equipment);

            $this->addReference('Equip_'.$k, $equipment);
        }

        // On déclenche l'enregistrement de toutes les catégories
        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 3;
    }
}