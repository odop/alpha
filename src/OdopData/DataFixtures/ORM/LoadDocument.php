<?php

namespace OdopData\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use OdopData\Entity\Document;

class LoadDocument extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // Liste des noms de catégorie à ajouter
        $documents = array(
            array('name' => 'default_profile_picture.jpg', 'type' => 'image/jpeg', 'path' => 'img/default_profilePicture.jpg', 'description' => 'Photo par défault'),
            array('name' => 'profil_1.jpg', 'type' => 'image/jpeg', 'path' => 'uploads/demo/profile_picture/profil_1.jpg', 'description' => 'Julien Decoen'),
            array('name' => 'profil_2.jpg', 'type' => 'image/jpeg', 'path' => 'uploads/demo/profile_picture/profil_2.jpg', 'description' => 'Jeremy Félicité'),
            array('name' => 'profil_3.jpg', 'type' => 'image/jpeg', 'path' => 'uploads/demo/profile_picture/profil_3.jpg', 'description' => 'Cyril Lopez'),
            array('name' => 'cat_1.jpg', 'type' => 'image/jpeg', 'path' => 'uploads/demo/categorys/cat_1.jpg', 'description' => 'Cat 1'),
            array('name' => 'cat_2.jpg', 'type' => 'image/jpeg', 'path' => 'uploads/demo/categorys/cat_2.jpg', 'description' => 'Cat 2'),
            array('name' => 'cat_3.jpg', 'type' => 'image/jpeg', 'path' => 'uploads/demo/categorys/cat_3.jpg', 'description' => 'Cat 3'),
            array('name' => 'cat_4.jpg', 'type' => 'image/jpeg', 'path' => 'uploads/demo/categorys/cat_4.jpg', 'description' => 'Cat 4'),
            array('name' => 'classified_1.jpg', 'type' => 'image/jpeg', 'path' => 'uploads/demo/classified/classified_1.jpg', 'description' => 'Classified 1'),
            array('name' => 'classified_2.jpg', 'type' => 'image/jpeg', 'path' => 'uploads/demo/classified/classified_2.jpg', 'description' => 'Classified 2'),
            array('name' => 'classified_3.jpg', 'type' => 'image/jpeg', 'path' => 'uploads/demo/classified/classified_3.jpg', 'description' => 'Classified 3'),
            array('name' => 'classified_4.jpg', 'type' => 'image/jpeg', 'path' => 'uploads/demo/classified/classified_4.jpg', 'description' => 'Classified 4'),
            array('name' => 'classified_5.jpg', 'type' => 'image/jpeg', 'path' => 'uploads/demo/classified/classified_5.jpg', 'description' => 'Classified 5'),
            array('name' => 'classified_6.jpg', 'type' => 'image/jpeg', 'path' => 'uploads/demo/classified/classified_6.jpg', 'description' => 'Classified 6'),
            array('name' => 'classified_7.jpg', 'type' => 'image/jpeg', 'path' => 'uploads/demo/classified/classified_7.jpg', 'description' => 'Classified 7'),
            array('name' => 'classified_8.jpg', 'type' => 'image/jpeg', 'path' => 'uploads/demo/classified/classified_8.jpg', 'description' => 'Classified 8'),
            array('name' => 'classified_9.jpg', 'type' => 'image/jpeg', 'path' => 'uploads/demo/classified/classified_9.jpg', 'description' => 'Classified 9'),
            array('name' => 'classified_10.jpg', 'type' => 'image/jpeg', 'path' => 'uploads/demo/classified/classified_10.jpg', 'description' => 'Classified 10'),
            array('name' => 'classified_11.jpg', 'type' => 'image/jpeg', 'path' => 'uploads/demo/classified/classified_11.jpg', 'description' => 'Classified 11'),
            array('name' => 'classified_12.jpg', 'type' => 'image/jpeg', 'path' => 'uploads/demo/classified/classified_12.jpg', 'description' => 'Classified 12'),
        );

        foreach ($documents as $k => $doc) {
            $document = new Document();
            $document->setName($doc['name']);
            $document->setType($doc['type']);
            $document->setPath($doc['path']);
            $document->setDescription($doc['description']);

            // On la persiste
            $manager->persist($document);

            $this->addReference("Doc_".$k, $document);
        }

        // On déclenche l'enregistrement de toutes les catégories
        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 1;
    }
}
