<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use OdopData\Entity\Equipment;
use AdminBundle\Form\EquipmentType;

/**
 * Equipment controller.
 *
 * @Route("admin/equipment")
 */
class EquipmentController extends Controller
{

    /**
     * Lists all Equipment entities.
     *
     * @Route("/{page}", name="admin_equipment_index", defaults={"page": 1})
     * @Method("GET")
     */
    public function indexAction(Request $request, $page)
    {
        $this->get('admin.breadcrumbs')->setBreadcrumbs(array("admin_equipment_index" => "Equipements"));

        $em = $this->getDoctrine()->getManager();

        $equipments = $em->getRepository(Equipment::class)->findAll();

        $paginator  = $this->get('knp_paginator');
        $result = $paginator->paginate(
            $equipments, /* query NOT result */
            $request->query->getInt('page', $page)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('AdminBundle:Equipment:index.html.twig', array(
            'equipments' => $result,
        ));
    }

    /**
     * Creates a new Equipment entity.
     *
     * @Route("/new", name="admin_equipment_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $this->get('admin.breadcrumbs')->setBreadcrumbs(
            array("admin_equipment_index" => "Equipements",
                "admin_equipment_new" => "New" ));

        $equipment = new Equipment();
        $form = $this->createForm('AdminBundle\Form\EquipmentType', $equipment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($equipment);
            $em->flush();

            $this->addFlash(
                'success',
                'Ajouté avec succès'
            );

            return $this->redirectToRoute('admin_equipment_show', array('id' => $equipment->getId()));
        }

        return $this->render('AdminBundle:Equipment:new.html.twig', array(
            'equipment' => $equipment,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Equipment entity.
     *
     * @Route("/{id}/show", name="admin_equipment_show")
     * @Method("GET")
     * @param Equipment $equipment
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Equipment $equipment)
    {
        $this->get('admin.breadcrumbs')->setBreadcrumbs(
            array("admin_equipment_index" => "Equipements",
                "admin_dashboard" => "Afficher" ));

        $deleteForm = $this->createDeleteForm($equipment);
        $form = $this->createForm('AdminBundle\Form\EquipmentType', $equipment);

        return $this->render('AdminBundle:Equipment:show.html.twig', array(
            'equipment' => $equipment,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Equipment entity.
     *
     * @Route("{id}/edit/", name="admin_equipment_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Equipment $equipment
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Equipment $equipment)
    {
        $this->get('admin.breadcrumbs')->setBreadcrumbs(
            array("admin_equipment_index" => "Equipements",
                "admin_dashboard" => "Editer" ));

        $deleteForm = $this->createDeleteForm($equipment);
        $form = $this->createForm('AdminBundle\Form\EquipmentType', $equipment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($equipment);
            $em->flush();

            $this->addFlash(
                'success',
                'Modifications enregistrées avec succès'
            );

            return $this->redirectToRoute('admin_equipment_edit', array('id' => $equipment->getId()));
        }

        return $this->render('AdminBundle:Equipment:edit.html.twig', array(
            'equipment' => $equipment,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Equipment entity.
     *
     * @Route("/{id}", name="admin_equipment_delete")
     * @Method("DELETE")
     * @param Request $request
     * @param Equipment $equipment
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Equipment $equipment)
    {
        $form = $this->createDeleteForm($equipment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($equipment);
            $em->flush();

            $this->addFlash(
                'success',
                'Supprimé avec succès'
            );
        }

        return $this->redirectToRoute('admin_equipment_index');
    }

    /**
     * Creates a form to delete a Equipment entity.
     *
     * @param Equipment $equipment The Equipment entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Equipment $equipment)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_equipment_delete', array('id' => $equipment->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }
}
