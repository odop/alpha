<?php

namespace AppBundle\Controller;

use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use OdopData\Entity\Comment;
use AppBundle\Form\CommentType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Comment controller.
 *
 * @Route("comment")
 */
class CommentController extends Controller
{
  /**
   * @Method({"GET"})
   * @Route("/show", name="show_comment")
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
   */
    public function showAction(Request $request, $comments = null, $classified = null)
    {
      $comment = new Comment();

      if (!$classified) {
        throw $this->createNotFoundException('Unable to find classified entity.');
      }

      $comment->setClassified($classified);

      $form = $this->createForm(CommentType::class, $comment);

      $form->handleRequest($request);

      return $this->render('AppBundle:Comment:show.html.twig', array(
          'comments' => $comments,
          'form' => $form->createView(),
      ));
    }


    /**
     * @Method({"POST"})
     * @Route("/new", name="add_comment")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
      $comment = new Comment();

      $form = $this->createForm(CommentType::class, $comment);

      $form->handleRequest($request);


      if ($form->isSubmitted() && $form->isValid()) {
          $em = $this->getDoctrine()->getManager();

          $user = $this->get('security.token_storage')->getToken()->getUser();
          $comment->setUser($user); // set the current user

          $em->persist($comment);
          $em->flush();

          return new JsonResponse(array('success' => 'Success', 'message' => 'Votre commentaire est maintenant en cours d\'approbation'), 200);
      }

      return new JsonResponse(array('error' => 'Error', 'message' => $form->getErrors()), 200);

    }
}
