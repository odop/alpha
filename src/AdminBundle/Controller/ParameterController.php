<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\ParameterContainerType;
use OdopData\Entity\Category;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use OdopData\Entity\Parameter;
use AdminBundle\Form\ParameterType;

/**
 * Parameter controller.
 *
 * @Route("admin/parameter")
 */
class ParameterController extends Controller
{
    /**
     * Lists all Parameter entities.
     *
     * @Route("/", name="admin_parameter_index")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $this->get('admin.breadcrumbs')->setBreadcrumbs(array("admin_parameter_index" => "Paramètres"));

        $parameters = $this->getAllParameter();
        $parameterCollection = new Parameter();

        foreach ($parameters as $parameter) {
            $parameterCollection->getParameters()->add($parameter);
        }

        $editForm = $this->createForm(ParameterContainerType::class, $parameterCollection);

        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            foreach ( $parameterCollection->getParameters() as $parameter){
                if ($parameter != null) {
                    $parameterCollection->getParameters()->removeElement($parameter);
                    $em->persist($parameter);
                }
            }
            $em->flush();

            $this->addFlash(
                'success',
                'Modifications enregistrées avec succès'
            );

//            return $this->redirectToRoute('admin_parameter_index');
        }

        return $this->render('AdminBundle:Parameter:show.html.twig', array(
            'editForm' => $editForm->createView(),
        ));
    }

    protected function getAllParameter(){
        $em = $this->getDoctrine()->getManager();
        $parameters = $em->getRepository(Parameter::class)->findByCategory();

        $lastParams = '';
//        foreach ($parameters as $params){

//            if($lastParams == $params->getCategory()->getId()){
//                $params->setCategory(new Category());
//            } else {
//                $lastParams = $params->getCategory()->getId();
//            }
//        }
//        return array('parameters' => $parameters);
        return $parameters;
    }

    /**
     * Creates a new Parameter entity.
     *
     * @Route("/new", name="admin_parameter_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $this->get('admin.breadcrumbs')->setBreadcrumbs(
            array("admin_parameter_index" => "Paramètres",
                "admin_parameter_new" => "Nouveau" ));

        $parameter = new Parameter();
        $form = $this->createForm('AdminBundle\Form\ParameterType', $parameter);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($parameter);
            $em->flush();

            $this->addFlash(
                'success',
                'Ajouté avec succès'
            );

            return $this->redirectToRoute('admin_parameter_show', array('id' => $parameter->getId()));
        }

        return $this->render('AdminBundle:Parameter:new.html.twig', array(
            'parameter' => $parameter,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Parameter entity.
     *
     * @Route("/{id}", name="admin_parameter_show")
     * @Method("GET")
     * @param Parameter $parameter
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Parameter $parameter)
    {
        $this->get('admin.breadcrumbs')->setBreadcrumbs(
            array("admin_parameter_index" => "Paramètres",
                "admin_dashboard" => "Afficher" ));

        $deleteForm = $this->createDeleteForm($parameter);
        $form = $this->createForm('AdminBundle\Form\ParameterType', $parameter);

        return $this->render('AdminBundle:Parameter:show.html.twig', array(
            'parameter' => $parameter,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Parameter entity.
     *
     * @Route("/{id}/edit", name="admin_parameter_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Parameter $parameter
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Parameter $parameter)
    {
        $this->get('admin.breadcrumbs')->setBreadcrumbs(
            array("admin_parameter_index" => "Paramètres",
                "admin_dashboard" => "Editer" ));

        $deleteForm = $this->createDeleteForm($parameter);
        $form = $this->createForm('AdminBundle\Form\ParameterType', $parameter);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($parameter);
            $em->flush();

            $this->addFlash(
                'success',
                'Modifications enregistrées avec succès'
            );

            return $this->redirectToRoute('admin_parameter_edit', array('id' => $parameter->getId()));
        }

        return $this->render('AdminBundle:Parameter:edit.html.twig', array(
            'parameter' => $parameter,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Parameter entity.
     *
     * @Route("/{id}", name="admin_parameter_delete")
     * @Method("DELETE")
     * @param Request $request
     * @param Parameter $parameter
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Parameter $parameter)
    {
        $form = $this->createDeleteForm($parameter);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($parameter);
            $em->flush();

            $this->addFlash(
                'success',
                'Supprimé avec succès'
            );
        }

        return $this->redirectToRoute('admin_parameter_index');
    }

    /**
     * Creates a form to delete a Parameter entity.
     *
     * @param Parameter $parameter The Parameter entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Parameter $parameter)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_parameter_delete', array('id' => $parameter->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
