<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use AppBundle\Form\DataTransformer\NumberIdTransformer;
use Doctrine\Common\Persistence\ObjectManager;
use OdopData\Entity\Reservation;


class ReservationType extends AbstractType
{

    /**
     * @var FormBuilderInterface $em
     */
     protected $em;

    /**
     * Constructor
     *
     * @param FormBuilderInterface $em
     */
    public function __construct(ObjectManager $em)
    {
        $this->em = $em;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateStart', DateTimeType::class, array(
                'widget' => 'single_text',
                'html5' => false,
                'attr' => ['data-provider' => 'date_start'],
                'format' => 'dd-MM-yyyy HH:mm'))
            ->add('dateEnd', DateTimeType::class, array(
                'widget' => 'single_text',
                'html5' => false,
                'attr' => ['data-provider' => 'date_end'],
                'format' => 'dd-MM-yyyy HH:mm'))
            ->add('participant', IntegerType::class)
            ->add('classified', HiddenType::class, array())
        ;

        $builder->get('classified')
            ->addModelTransformer(new NumberIdTransformer($this->em, 'OdopData:Classified'));
    }

    public function getName()
    {
        return 'reservationtype';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Reservation::class,
        ));
    }
}
