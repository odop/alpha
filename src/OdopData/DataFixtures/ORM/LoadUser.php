<?php

namespace OdopData\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use OdopData\Entity\User;

class LoadUser extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // Liste des noms de catégorie à ajouter
        $users = array(
            array('firstname' => 'Julien',
                'lastname' => 'Decoen',
                'email' => 'julien.decoen@gmail.com',
                'password' => '$2y$10$LyTegN8hXJTugtxg08yLCecrxLArpD4vrNYYKd3ZlVvpbrS1npsaa',
                'city' => 'Conflans',
                'country' => 'France',
                'roles' => array('ROLE_ADMIN'),
                'picture' => $this->getReference('Doc_1'),
            ),
            array('firstname' => 'Jérémy',
                'lastname' => 'Félicite',
                'email' => 'jeremy.felicite@gmail.com',
                'password' => '$2y$13$bVlyi6O5qZDnSnk6peN1f.mcnkVle6JaTqE9S/wqz1/M0GvcT3TFW',
                'city' => 'Conflans',
                'country' => 'France',
                'roles' => array('ROLE_ADMIN'),
                'picture' => $this->getReference('Doc_2'),
            ),
            array('firstname' => 'Cyril',
                'lastname' => 'Lopez',
                'email' => 'j.felicite@gmail.com',
                'password' => '$2y$13$bVlyi6O5qZDnSnk6peN1f.mcnkVle6JaTqE9S/wqz1/M0GvcT3TFW',
                'city' => 'Conflans',
                'country' => 'France',
                'roles' => array('ROLE_ADMIN'),
                'picture' => $this->getReference('Doc_3'),
            ),
        );

        foreach ($users as $k => $usr) {
            $user = new User();
            $user->setFirstname($usr['firstname']);
            $user->setLastname($usr['lastname']);
            $user->setEmail($usr['email']);
            $user->setPassword($usr['password']);
            $user->setCity($usr['city']);
            $user->setCountry($usr['country']);
            $user->setRoles($usr['roles']);
            if($usr['picture'])
                $user->setProfilePicture($usr['picture']);
            $user->setEnabled(true);

            // On la persiste
            $manager->persist($user);

            $this->addReference('User_'.$k, $user);
        }

        // On déclenche l'enregistrement de toutes les catégories
        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 4;
    }
}
