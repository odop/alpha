<?php

namespace AdminBundle\Controller;

use OdopData\Entity\Document;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use OdopData\Entity\User;
use AdminBundle\Form\UserType;

/**
 * User controller.
 *
 * @Route("admin/user")
 */
class UserController extends Controller
{
    /**
     * Lists all User entities.
     *
     * @Route("/{page}", name="admin_user_index", defaults={"page": 1})
     * @Route("/{filter}/{page}", name="admin_user_index_by",
     *    requirements={"filter": "register|owner|admin"},
     *    defaults={"page": 1})
     * @Method("GET")
     * @param string $filter
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, $filter = "", $page)
    {
        $this->get('admin.breadcrumbs')->setBreadcrumbs(array("admin_user_index" => "Utilisateurs"));
        $em = $this->getDoctrine()->getManager();

        if($filter) {
            switch($filter){
                case 'register':
                    $filter = 3;
                    break;
                case 'owner':
                    $filter = 2;
                    break;
                case 'admin':
                    $filter = 1;
                    break;
                default:
                    break;
            }
            $users = $em->getRepository(User::class)->findBy(array('roles' => $filter));
        } else
            $users = $em->getRepository(User::class)->findAll();

        $paginator  = $this->get('knp_paginator');
        $result = $paginator->paginate(
            $users, /* query NOT result */
            $request->query->getInt('page', $page)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('AdminBundle:User:index.html.twig', array(
            'users' => $result,
        ));
    }

    /**
     * Creates a new User entity.
     *
     * @Route("/new", name="admin_user_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $this->get('admin.breadcrumbs')->setBreadcrumbs(
            array("admin_user_index" => "Utilisateurs",
                "admin_user_new" => "Nouveau" ));

        $user = new User();
        $form = $this->createForm('AdminBundle\Form\UserType', $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            if($file = $user->getBrochure()) {
                $fileName = $this->get('app.profilePicture_uploader')->upload($file);
                $user->setBrochure($fileName);
            }

            $em->persist($user);
            $em->flush();

            $this->addFlash(
                'success',
                'Ajouté avec succès'
            );

            return $this->redirectToRoute('admin_user_show', array('id' => $user->getId()));
        }

        return $this->render('AdminBundle:User:new.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a User entity.
     *
     * @Route("/{id}/show", name="admin_user_show")
     * @Method("GET")
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(User $user)
    {
        $this->get('admin.breadcrumbs')->setBreadcrumbs(
            array("admin_user_index" => "Utilisateurs",
                "admin_dashboard" => "Afficher" ));

        $deleteForm = $this->createDeleteForm($user);
        $form = $this->createForm('AdminBundle\Form\UserType', $user);

        return $this->render('AdminBundle:User:show.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/{id}/edit", name="admin_user_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, User $user)
    {
        $this->get('admin.breadcrumbs')->setBreadcrumbs(
            array("admin_user_index" => "Utilisateurs",
                "admin_dashboard" => "Editer" ));

        $deleteForm = $this->createDeleteForm($user);
        $form = $this->createForm('AdminBundle\Form\UserType', $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash(
                'success',
                'Modifications enregistrées avec succès'
            );

            return $this->redirectToRoute('admin_user_edit', array('id' => $user->getId()));
        }

//        dump($user);die();

        return $this->render('AdminBundle:User:edit.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a User entity.
     *
     * @Route("/{id}", name="admin_user_delete")
     * @Method("DELETE")
     * @param Request $request
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, User $user)
    {
        $form = $this->createDeleteForm($user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();

            $this->addFlash(
                'success',
                'Supprimé avec succès'
            );
        }

        return $this->redirectToRoute('admin_user_index');
    }

    /**
     * Creates a form to delete a User entity.
     *
     * @param User $user The User entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(User $user)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_user_delete', array('id' => $user->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }


    /**
     *
     * @Method({"GET", "POST"})
     * @Route("/ajax/snippet/image/send", name="ajax_snippet_image_send")
     * @param Request $request
     * @return JsonResponse
     */
    public function ajaxSnippetImageSendAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $document = new Document();
        $user = $em->getRepository('AdminBundle:User')->find($request->request->get('target_id'));

        $media = $request->files->get('file');

        $document->setUploadDir('profile_picture');
        $document->setFile($media);
        $document->setPath($media->getPathName());
        $document->setName($media->getClientOriginalName());
        $em->persist($document);

        $user->setProfilePicture($document);
        $em->persist($user);

        $em->flush();

        return new JsonResponse(array('success' => true));
    }
}
