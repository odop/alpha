# Odop (alpha)

## Installation

1. Install composer & npm

2. Run the following commands :

```bash
$ composer update
$ npm install
$ gulp
$ php bin/console doctrine:generate:entities OdopData
$ php bin/console doctrine:schema:update --force
$ php bin/console doctrine:fixtures:load --fixtures=src/OdopData/DataFixtures/ORM --append
```

3. Access to the website at the address <http://localhost/app_dev.php/>
