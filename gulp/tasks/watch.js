var gulp        = require('gulp');
var paths       = require('../config');
var runSequence = require('run-sequence');

gulp.task('watch', function(callback) {
  gulp.watch(paths.assets.all, ['assets']);
  gulp.watch(paths.styles.all, function() {
    runSequence('styles', 'bs-reload');
  });
});
