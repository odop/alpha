<?php

namespace OdopData\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use OdopData\Entity\Category;

class LoadCategory extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $type_space = $type_location = $site_params = $type_event = 0;

        // Liste des noms de catégorie à ajouter
        $categorys = array(
            array('name' => 'Paramètres généraux', 'type' => 'site_params'),
            array('name' => 'Bloc Highlight', 'type' => 'site_params'),
            array('name' => 'Intérieur', 'type' => 'type_space'),
            array('name' => 'Extérieur', 'type' => 'type_space'),
            array('name' => 'Mixte', 'type' => 'type_space'),
            array('name' => 'Logement entier', 'type' => 'type_location'),
            array('name' => 'Pièce unique', 'type' => 'type_location'),
            array('name' => 'Ensemble de pièce', 'type' => 'type_location'),
            array('name' => 'Garden Party', 'type' => 'type_event', 'picture' => $this->getReference('Doc_4')),
            array('name' => 'Soirée film', 'type' => 'type_event', 'picture' => $this->getReference('Doc_5')),
            array('name' => 'Atelier cuisine', 'type' => 'type_event', 'picture' => $this->getReference('Doc_6')),
            array('name' => 'Tournage', 'type' => 'type_event', 'picture' => $this->getReference('Doc_7')),
        );

        foreach ($categorys as $k => $cat) {
            $category = new Category();
            $category->setName($cat['name']);
            $category->setType($cat['type']);

            // On la persiste
            $manager->persist($category);

            switch ($cat['type']){
                case 'type_space':
                    $this->addReference('Type_space_' . $type_space++, $category);
                    break;
                case 'type_location':
                    $this->addReference('Type_location_' . $type_location++, $category);
                    break;
                case 'site_params':
                    $this->addReference('Site_params_' . $site_params++, $category);
                    break;
                case 'type_event':
                    $category->setPicture($cat['picture']);
                    $this->addReference('Type_event_' . $type_event++, $category);
                    break;
            }
        }

        // On déclenche l'enregistrement de toutes les catégories
        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 2;
    }
}