<?php
/**
 * Created by PhpStorm.
 * User: Bobby
 * Date: 31/01/2017
 * Time: 23:00
 */

namespace AppBundle\Service;

use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use OdopData\Entity\User;
use OdopData\Entity\Reservation;
use OdopData\Entity\Comment;

class OdopCreationListener
{
    /**
     * @var OdopMailer
     */
    private $odopMailer;

    public function __construct(OdopMailer $odopMailer)
    {
        $this->odopMailer = $odopMailer;
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        if ( $entity instanceof Reservation ){
            $this->odopMailer->sendNotificationReservation($entity);
        }
        else if ( $entity instanceof Comment ){
            $this->odopMailer->sendNotificationComment($entity);
        }
        else{
            return;
        }
    }
}