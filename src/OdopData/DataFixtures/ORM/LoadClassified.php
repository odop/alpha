<?php

namespace OdopData\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use OdopData\Entity\Classified;

class LoadClassified extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // Liste des noms de catégorie à ajouter
        $classifieds = array(
            array('title' => 'Maison de caractère, beau jardin et belle piscine',
                'country' => 'France',
                'region' => 'Auvergne-Rhône-Alpes',
                'city' => 'Labeaume',
                'street' => '13 route du puits',
                'location_lat' => '44.448413',
                'location_lng' => '4.307094000000006',
                'description' => 'Les atouts de notre maison:
                                La piscine au milieu du jardin planté de chênes verts et d\'iris.
                                Mobilier de jardin (plusieurs tables dont une pour 8 personnes, chaises, fauteuils, transats...) et barbecues vous permettent de profiter des 5000 m2 de ce dernier
                                Le couradou, galerie transformée avec de grandes baies vitrées en pièce à vivre et salle à manger permettant d\'admirer le jardin, la piscine et la nature tout au long de l\'année.',
                'surfaceArea' => 120,
                'maxParticipant' => 70,
                'price' => '150',
                'categorys' => array($this->getReference('Type_space_1'), $this->getReference('Type_location_0'), $this->getReference('Type_event_0')),
                'equipment' => array($this->getReference('Equip_1'),$this->getReference('Equip_5')),
                'pictures' => array($this->getReference('Doc_8'), $this->getReference('Doc_9'), $this->getReference('Doc_10')),
                'owner' => $this->getReference('User_1'),
                ),
            array('title' => 'Studio spacieux avec mezzanine',
                'country' => 'France',
                'region' => 'Grand Est',
                'street' => '13 route du puits',
                'city' => 'Schiltigheim',
                'location_lat' => '48.60522599999999',
                'location_lng' => '7.7484530000000404',
                'description' => 'Proche des institutions UE, du Palais des Congrès, et du Parc des Expositions du Wacken',
                'surfaceArea' => 70,
                'maxParticipant' => 20,
                'price' => '15',
                'categorys' => array($this->getReference('Type_space_0'), $this->getReference('Type_location_1'), $this->getReference('Type_event_2')),
                'equipment' => array($this->getReference('Equip_0'), $this->getReference('Equip_4')),
                'pictures' => array($this->getReference('Doc_8'), $this->getReference('Doc_9'), $this->getReference('Doc_10')),
                'owner' => $this->getReference('User_2'),
                ),
            array('title' => 'Duplex de charme avec terrasse proche Vieux Port',
                'country' => 'France',
                'region' => 'Nouvelle-Aquitaine',
                'city' => 'La Rochelle',
                'street' => '13 route du puits',
                'location_lat' => '46.160329',
                'location_lng' => '-1.1511390000000574',
                'description' => 'Mon logement est proche de le centre ville, la vue exceptionnelle, l\'art et la culture, parcs et les restaurants. Vous apprécierez mon logement pour la hauteur des plafonds, la vue et la cuisine. Mon logement est parfait pour les couples, les voyageurs en solo, les voyageurs d\'affaires, les familles (avec enfants) et les grands groupes.',
                'surfaceArea' => 100,
                'maxParticipant' => 40,
                'price' => '25',
                'categorys' => array($this->getReference('Type_space_2'), $this->getReference('Type_location_0'), $this->getReference('Type_event_3')),
                'equipment' => array($this->getReference('Equip_4'),$this->getReference('Equip_5')),
                'pictures' => array($this->getReference('Doc_11'), $this->getReference('Doc_12'), $this->getReference('Doc_13')),
                'owner' => $this->getReference('User_0'),
                ),
            array('title' => 'Beau voilier de 11m50 amarré dans le vieux port',
                'country' => 'France',
                'region' => 'Nouvelle-Aquitaine',
                'city' => 'La Rochelle',
                'street' => '13 route du puits',
                'location_lat' => '46.160329',
                'location_lng' => '-1.1511390000000574',
                'description' => 'A bord d\'un voilier de 11m50, en plein cœur de La Rochelle, dans le vieux port, face à la grande horloge... Venez passer un moment à bord, en famille ou en amoureux.
                                Ambiance de port, apéritif sur l\'eau, dans un cadre historique, avec vue sur les deux magnifiques tours de la Rochelle.
                                Vous serez proche de nombreux bars, restaurants, terrasses et commerces..
                                La très sympathique plage de la concurrence est à 3 mn à pieds.',
                'surfaceArea' => 50,
                'maxParticipant' => 15,
                'price' => '20',
                'categorys' => array($this->getReference('Type_space_0'), $this->getReference('Type_location_1'), $this->getReference('Type_event_1'), $this->getReference('Type_event_0')),
                'equipment' => array($this->getReference('Equip_2'),$this->getReference('Equip_3')),
                'pictures' => array($this->getReference('Doc_14'), $this->getReference('Doc_15'), $this->getReference('Doc_16')),
                'owner' => $this->getReference('User_1'),
                ),
            array('title' => 'Espace cosy en plein coeur de Bordeaux',
                'country' => 'France',
                'region' => 'Nouvelle-Aquitaine',
                'city' => 'Bordeaux',
                'street' => '13 route du puits',
                'location_lat' => '44.837789',
                'location_lng' => '-0.5791799999999512',
                'description' => 'Mon logement est proche de La place de la Victoire, le marché des capucins, la place St Michel, la rue Sainte Catherine, le musée d\'Aquitaine, la cathédrale Pey Berland, la vie nocturne, les transports en commun.... Vous apprécierez mon logement pour le lit confortable, le confort, le charme de l\'ancien rénové, la pierre de taille Bordelaise, la baignoire d\'angle, espace cosy, chaleureux.. Mon logement est parfait pour les couples, les voyageurs en solo et les voyageurs d\'affaires.',
                'surfaceArea' => 25,
                'maxParticipant' => 10,
                'price' => '15',
                'categorys' => array($this->getReference('Type_space_0'), $this->getReference('Type_location_0'), $this->getReference('Type_event_0')),
                'equipment' => array($this->getReference('Equip_1'),$this->getReference('Equip_0')),
                'pictures' => array($this->getReference('Doc_8'), $this->getReference('Doc_9'), $this->getReference('Doc_10')),
                'pictures' => array($this->getReference('Doc_17'), $this->getReference('Doc_18'), $this->getReference('Doc_19')),
                'owner' => $this->getReference('User_2'),
                ),
        );

        foreach ($classifieds as $k => $cls) {
            $classified = new Classified();
            $classified->setTitle($cls['title']);
            $classified->setCountry($cls['country']);
            $classified->setRegion($cls['region']);
            $classified->setCity($cls['city']);
            $classified->setStreet($cls['street']);
            $classified->setLocationLat($cls['location_lat']);
            $classified->setLocationLng($cls['location_lng']);
            $classified->setDescription($cls['description']);
            $classified->setSurfaceArea($cls['surfaceArea']);
            $classified->setMaxParticipant($cls['maxParticipant']);
            $classified->setPrice($cls['price']);
            $classified->setOwner($cls['owner']);
            foreach ($cls['categorys'] as $category){
                $classified->addCategory($category);
            }
            foreach ($cls['equipment'] as $equipment){
                $classified->addEquipment($equipment);
            }
            foreach ($cls['pictures'] as $picture){
                $classified->addPicture($picture);
            }

            // On la persiste
            $manager->persist($classified);


            $this->addReference('Classified_'.$k, $classified);
        }

        // On déclenche l'enregistrement de toutes les catégories
        $manager->flush();

    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 5;
    }
}
