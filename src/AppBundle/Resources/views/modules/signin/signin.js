export default function signin() {

      $(document).ready(function () {
          let signin_form = $('#signin_form');

          $("#signin-trigger").click(function(){
              $('#loginModal').modal('hide');
              $('#signinModal').modal('show');
          });

          $('#signinModal .login-submit').on("click", function (e) {
              e.preventDefault();
              $('#signinModal .login-submit').html('<i class="fa fa-spinner fa-spin"></i>');

              $.ajax({
                  type: signin_form.attr('method'),
                  url: signin_form.attr('action'),
                  data: signin_form.serialize(),
                  success: function(response) {
                    if(response.success) {
                        $('#signinModal .login-submit').hide();
                        $('#signinModal .alert-success').removeClass("hidden");
                        $('#signinModal .alert-success .alert-message').html(response.message);
                        $(".message-error").remove();
                        $("input").parent().removeClass("has-error");
                    } else {
                        showFormError(response.errors);
                        $('#signinModal .login-submit').html("S'inscrire");
                    }
                  }
              });
          });

          function showFormError(data) {

            $(".message-error").remove();
            $("input").parent().removeClass("has-error");
            for(var key in data){
                if(key === 'plainPassword'){
                    $(".fos_user_registration_form_" + key).parent().addClass('has-error').before('<span class="message-error" style="color:#a94442"><i class="fa fa-times fa-fw" aria-hidden="true" style="color:#a94442"></i>'+ data[key].first +'</span>');
                }
                $("#fos_user_registration_form_" + key).parent().addClass('has-error').before('<span class="message-error" style="color:#a94442"><i class="fa fa-times fa-fw" aria-hidden="true" style="color:#a94442"></i>'+ data[key] +'</span>');
            }
          }


      });
}

signin.selector = '#signinModal';
