<?php

namespace OdopData\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="OdopData\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class User extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     *  @var string
     *  @Assert\NotBlank(message="Entrez un prénom s'il vous plait.")
     *  @Assert\Length(
     *  min = "2",
     *  minMessage = "Votre prénom doit contenir au minimum 2 caractères",
     * )
     * @ORM\Column(name="firstname", type="string", length=255, nullable=true)
     */

    private $firstname;

    /**
     * @var string
     *  @Assert\NotBlank(message="Entrez un nom s'il vous plait.")
     *  @Assert\Length(
     *  min = "2",
     *  minMessage = "Votre nom doit contenir au minimum 2 caractères",
     * )
     * @ORM\Column(name="lastname", type="string", length=255, nullable=true)
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255, nullable=true)
     */
    private $country;

    /**
     * @ORM\ManyToOne(targetEntity="OdopData\Entity\Document", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $profilePicture;

    /**
     * @ORM\OneToMany(targetEntity="OdopData\Entity\Classified", mappedBy="owner")
     * @ORM\JoinColumn(nullable=true)
     */
    private $classifieds;

    /**
    * @ORM\OneToMany(targetEntity="OdopData\Entity\Comment", mappedBy="user")
    * @ORM\JoinColumn(nullable=true)
    */
    private $comments;

    /**
     * @ORM\OneToMany(targetEntity="OdopData\Entity\Reservation", mappedBy="user")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $reservations;

    /**
     * @ORM\ManyToMany(targetEntity="OdopData\Entity\Conversation", mappedBy="users")
     * @ORM\JoinColumn(nullable=true)
     *
     */
    private $conversations;

    /**
     * @var \DateTime
     *@Gedmo\Timestampable(on="create")
     * @ORM\Column(name="date_created", type="datetime")
     */
    private $dateCreated;

    /**
     * @var \DateTime
     *@Gedmo\Timestampable(on="update")
     * @ORM\Column(name="date_modified", type="datetime", nullable=true)
     */
    private $dateModified;

    /**
     * @var string
     *
     * @ORM\Column(name="facebookID", type="string", nullable=true)
     */
    private $facebookID;

    /**
     * @ORM\Column(name="facebook_access_token", type="string", length=255, nullable=true) */
    protected $facebook_access_token;

    /**
     * @var string
     *
     * @ORM\Column(name="googleID", type="string", nullable=true)
     */
    private $googleID;

    /**
     * @ORM\Column(name="google_access_token", type="string", length=255, nullable=true)
     */
    protected $google_access_token;

    /**
     * @ORM\Column(name="avatar", type="string", length=255, nullable=true)
     */
    protected $avatar;

    // /**
    //  * @ORM\Column(name="is_active", type="boolean")
    //  */
    // private $isActive;

    public function __construct()
    {
        parent::__construct();
        // may not be needed, see section on salt below
        // $this->salt = md5(uniqid(null, true));
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            // $this->email,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize()
     * @param $serialized
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            // $this->email,
            $this->password,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    public function setEmail($email){
        parent::setEmail($email);
        parent::setUsername($email);
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return User
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return User
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    public function getBrochure()
    {
        return $this->profilePicture;
    }

    public function setBrochure($profilePicture)
    {
        $this->profilePicture = $profilePicture;

        return $this;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return User
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set dateModified
     *
     * @param \DateTime $dateModified
     *
     * @return User
     */
    public function setDateModified($dateModified)
    {
        $this->dateModified = $dateModified;

        return $this;
    }

    /**
     * Get dateModified
     *
     * @return \DateTime
     */
    public function getDateModified()
    {
        return $this->dateModified;
    }

    /**
     * @ORM\PreUpdate
     */
    public function updateDate()
    {
        $this->setDateModified(new \Datetime());
    }

    /**
     * @return mixed
     */
    public function __toString() {
        return strval($this->id);
    }

    /**
     * Set profilePicture
     *
     * @param string $profilePicture
     *
     * @return User
     */
    public function setProfilePicture($profilePicture)
    {
        $this->profilePicture = $profilePicture;

        return $this;
    }

    /**
     * Get profilePicture
     *
     * @return string
     */
    public function getProfilePicture()
    {
        return $this->profilePicture;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set classifieds
     *
     * @param \OdopData\Entity\Classified $classifieds
     *
     * @return User
     */
    public function setClassifieds($classifieds = null)
    {
        $this->classifieds = $classifieds;

        return $this;
    }

    /**
     * Get classifieds
     *
     * @return \OdopData\Entity\Classified
     */
    public function getClassifieds()
    {
        return $this->classifieds;
    }

    /**
     * Add classified
     *
     * @param \OdopData\Entity\Classified $classified
     *
     * @return User
     */
    public function addClassified(\OdopData\Entity\Classified $classified)
    {
        $this->classifieds[] = $classified;

        return $this;
    }

    /**
     * Remove classified
     *
     * @param \OdopData\Entity\Classified $classified
     */
    public function removeClassified(\OdopData\Entity\Classified $classified)
    {
        $this->classifieds->removeElement($classified);
    }

    /**
     * Get classified
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClassified()
    {
        return $this->classifieds;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param mixed $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return mixed
     */
    public function getReservation()
    {
        return $this->reservations;
    }

    /**
     * @param mixed $reservation
     */
    public function setReservation($reservation)
    {
        $this->reservations = $reservation;
    }

    /**
     * Add comment
     *
     * @param \OdopData\Entity\Comment $comment
     *
     * @return User
     */
    public function addComment(\OdopData\Entity\Comment $comment)
    {
        $this->comment[] = $comment;
        return $this;
    }

    /**
     * Remove comment
     *
     * @param \OdopData\Entity\Comment $comment
     */
    public function removeComment(\OdopData\Entity\Comment $comment)
    {
        $this->comment->removeElement($comment);
    }

    /**
     * Add reservation
     *
     * @param \OdopData\Entity\Reservation $reservation
     *
     * @return User
     */
    public function addReservation(\OdopData\Entity\Reservation $reservation)
    {
        $this->reservation[] = $reservation;

        return $this;
    }

    /**
     * Remove reservation
     *
     * @param \OdopData\Entity\Reservation $reservation
     */
    public function removeReservation(Reservation $reservation)
    {
        $this->reservation->removeElement($reservation);
    }

    /**
     * Add conversation
     *
     * @param \OdopData\Entity\Conversation $conversation
     *
     * @return User
     */
    public function addConversation(Conversation $conversation)
    {
        $this->conversations[] = $conversation;

        return $this;
    }

    /**
     * Remove conversation
     *
     * @param \OdopData\Entity\Conversation $conversation
     */
    public function removeConversation(Conversation $conversation)
    {
        $this->conversations->removeElement($conversation);
    }

    /**
     * Get conversations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConversations()
    {
        return $this->conversations;
    }

    /**
     * Set facebookID
     *
     * @param string $facebookID
     *
     * @return User
     */
    public function setFacebookID($facebookID)
    {
        $this->facebookID = $facebookID;

        return $this;
    }

    /**
     * Get facebookID
     *
     * @return string
     */
    public function getFacebookID()
    {
        return $this->facebookID;
    }

    /**
     * Set googleID
     *
     * @param string $googleID
     *
     * @return User
     */
    public function setGoogleID($googleID)
    {
        $this->googleID = $googleID;

        return $this;
    }

    /**
     * Get googleID
     *
     * @return string
     */
    public function getGoogleID()
    {
        return $this->googleID;
    }

    /**
     * Set facebookAccessToken
     *
     * @param string $facebookAccessToken
     *
     * @return User
     */
    public function setFacebookAccessToken($facebookAccessToken)
    {
        $this->facebook_access_token = $facebookAccessToken;

        return $this;
    }

    /**
     * Get facebookAccessToken
     *
     * @return string
     */
    public function getFacebookAccessToken()
    {
        return $this->facebook_access_token;
    }

    /**
     * Set googleAccessToken
     *
     * @param string $googleAccessToken
     *
     * @return User
     */
    public function setGoogleAccessToken($googleAccessToken)
    {
        $this->google_access_token = $googleAccessToken;

        return $this;
    }

    /**
     * Get googleAccessToken
     *
     * @return string
     */
    public function getGoogleAccessToken()
    {
        return $this->google_access_token;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     *
     * @return User
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Get reservations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReservations()
    {
        return $this->reservations;
    }
}
