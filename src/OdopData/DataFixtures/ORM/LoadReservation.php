<?php

namespace OdopData\DataFixtures\ORM;

use OdopData\Entity\Classified;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use OdopData\Entity\Reservation;

class LoadReservation extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // Liste des noms de catégorie à ajouter
        $reservations = array(
            array('dateStart' => new \DateTime("11-11-2016"),
                'dateEnd' => new \DateTime("12-12-2016"),
                'classified' => $this->getReference('Classified_1'),
                'user' => $this->getReference('User_0'),
                'status' => 'completed',
                'participant' => '12',
                'price' => '60'
            ),
            array('dateStart' => new \DateTime("11-11-2016"),
                'dateEnd' => new \DateTime("12-12-2016"),
                'classified' => $this->getReference('Classified_2'),
                'user' => $this->getReference('User_1'),
                'status' => 'completed',
                'participant' => '12',
                'price' => '60'
            ),
            array('dateStart' => new \DateTime("11-11-2016"),
                'dateEnd' => new \DateTime("12-12-2016"),
                'classified' => $this->getReference('Classified_3'),
                'user' => $this->getReference('User_0'),
                'status' => 'valide',
                'participant' => '6',
                'price' => '30'
            ),
            array('dateStart' => new \DateTime("11-11-2016"),
                'dateEnd' => new \DateTime("12-12-2016"),
                'classified' => $this->getReference('Classified_4'),
                'user' => $this->getReference('User_0'),
                'status' => 'pending',
                'participant' => '18',
                'price' => '90'
            ),
            array('dateStart' => new \DateTime("11-11-2016"),
                'dateEnd' => new \DateTime("12-12-2016"),
                'classified' => $this->getReference('Classified_3'),
                'user' => $this->getReference('User_0'),
                'status' => 'pending',
                'participant' => '24',
                'price' => '120'
            ),
            array('dateStart' => new \DateTime("11-06-2016"),
                'dateEnd' => new \DateTime("12-06-2016"),
                'classified' => $this->getReference('Classified_1'),
                'user' => $this->getReference('User_1'),
                'status' => 'valide',
                'participant' => '30',
                'price' => '150'
            ),
            array('dateStart' => new \DateTime("11-04-2016"),
                'dateEnd' => new \DateTime("12-04-2016"),
                'classified' => $this->getReference('Classified_2'),
                'user' => $this->getReference('User_1'),
                'status' => 'valide',
                'participant' => '36',
                'price' => '180'
            ),
        );

        foreach ($reservations as $res) {
            $reservation = new Reservation();
            $reservation->setDateStart($res['dateStart']);
            $reservation->setDateEnd($res['dateEnd']);
            $reservation->setClassified($res['classified']);
            $reservation->setUser($res['user']);
            $reservation->setStatus($res['status']);
            $reservation->setParticipant($res['participant']);
            $reservation->setPrice($res['price']);

            // On la persiste
            $manager->persist($reservation);
        }

        // On déclenche l'enregistrement de toutes les catégories
        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 6;
    }
}
