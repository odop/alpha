<?php
namespace AppBundle\Service\Calculator;

class Calculator
{

    /**
     * Square a number
     * @param $number
     * @return mixed
     */
    public function square($number){
        return $number*$number;
    }

}