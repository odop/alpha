var gulp = require('gulp');
var del  = require('del');

// cleanup tmp files
gulp.task('clean', () =>
  del('web/assets/', {dot: true})
);
