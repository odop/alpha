<?php
// src/OC/PlatformBundle/Email/ApplicationMailer.php

namespace AppBundle\Service;

use OdopData\Entity\User;
use OdopData\Entity\Comment;
use OdopData\Entity\Reservation;
use Symfony\Component\HttpFoundation\Request;

class OdopMailer
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;
    private $route;

    public function __construct(\Swift_Mailer $mailer, \Symfony\Bundle\FrameworkBundle\Routing\Router $route )
    {
        $this->mailer = $mailer;
        $this->route = $route;
    }

    public function sendNotificationReservation(Reservation $reservation){

        $message = new \Swift_Message('Demande de réservation');
        $message
            ->setFrom(array('labob91560@gmail.com' => 'Odop'))
            ->setTo($reservation->getClassified()->getOwner()->getEmail())
            ->setBody(
                'Bonjour,<br>Nous avons reçu une demande de réservation de la part de <b>' .
                $reservation->getUser()->getFirstname() . ' ' .  $reservation->getUser()->getLastname() .
                '</b> pour votre annonce : <b>' . $reservation->getClassified()->getTitle() . '</b><br>
                Vous pouvez dès à présent vous connecter sur Odop.fr pour valider la réservation<br><br>
                L\'équipe d\'Odop vous remercie',
                'text/html'
            )
        ;
        $this->mailer->send($message);
    }

    public function sendNotificationComment(Comment $comment){
        $message = new \Swift_Message('Un commentaire a été posté sur votre annonce');
        $message
            ->setFrom(array('labob91560@gmail.com' => 'Odop'))
            ->setTo($comment->getClassified()->getOwner()->getEmail())
            ->setBody(
                'Bonjour,<br>Un commentaire a été posté de la part de <b>' .
                $comment->getUser()->getFirstname(). ' ' . $comment->getUser()->getLastname() .
                ': "'. $comment->getContent() .'"
                </b> pour votre annonce : <b>' . $comment->getClassified()->getTitle() . '</b><br><br>
                L\'équipe d\'Odop vous remercie',
                'text/html'
            )
        ;
        $this->mailer->send($message);
    }
}
