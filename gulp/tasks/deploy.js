try {

  var gulp        = require('gulp');

  var git         = require('gulp-git');
  var bump        = require('gulp-bump');
  var tag_version = require('gulp-tag-version');
  var runSequence = require('run-sequence');
  var jenkinsapi  = require('jenkins-api');
  var argv        = require('yargs').argv;


  var url = 'https://' + process.env.JENKINS_USER + ':' + process.env.JENKINS_PASSWORD + '@jenkins.admin.axaxx.nu/jenkins';

  var jenkins = undefined;

  function getJenkinsInstance() {
    return jenkins ? jenkins : jenkinsapi.init(url, {strictSSL: false});
  }

  gulp.task('checkout-dev', function(cb) {

    git.checkout('develop', function(err) {
      if (err) throw err;
      cb();
    });
  });


  gulp.task('checkout-uat', function(cb) {

    git.checkout('uat', function(err) {
      if (err) throw err;
      cb();
    });
  });


  // bump patch in package.json + git tag
  gulp.task('bump', function() {

    // get all the files to bump version in
    return gulp.src(['./package.json'])
      // bump the version number in those files
      .pipe(bump({type: 'minor'}))

      // save it back to filesystem
      .pipe(gulp.dest('./'))

      // commit the changed version number
      .pipe(git.commit(':package: bumps package version'))

      // **tag it in the repository**
      .pipe(tag_version({ prefix: 'release-' }));

  });

  gulp.task('MEU', function(cb) {

    git.push('origin', 'develop', {args: '--tags'}, function(err) {
      if (err) throw err;

      git.checkout('uat', function(err) {
        if (err) throw err;

        git.pull('origin', 'uat', function(err) {
          if (err) throw err;

          git.merge('develop', {args: '--commit'}, function(err) {
            if (err) throw err;

            git.push('origin', 'uat', function(err) {
              if (err) throw err;
              cb();
            });
          });
        });
      });
    });
  });


  gulp.task('MEP', function(cb) {

    git.pull('origin', 'uat', function(err) {
      if (err) throw err;

      git.checkout('master', function(err) {
        if (err) throw err;

        git.pull('origin', 'master', function(err) {
          if (err) throw err;

          git.merge('uat', {args: '--commit'}, function(err) {
            if (err) throw err;

            git.push('origin', 'master', function(err) {
              if (err) throw err;
              cb();
            });
          });
        });
      });
    });
  });

  /* run jenkins job*/
  gulp.task('jenkins', function(cb) {

    var env = '', task = '';

    if (argv.env === 'uat') {
      env = 'uat';
      task = 'axacom_builddeploy';
    } else if (argv.env === 'preprod') {
      env = 'preprod';
      task = 'axacom_deploy';
    } else if (argv.env === 'prod') {
      env = 'production';
      task = 'axacom_deploy';
    }

    var packagejson = require('../../package.json');

    var params = { module: 'node', type: 'frontend', application: packagejson.name, release: packagejson.version, location: env };

    var jenkinsInstance = getJenkinsInstance();

    jenkinsInstance.build(task, params, function(err, data) {
      if (err) { return console.log(err); }
      console.log(data.message, 'https://jenkins.admin.axaxx.nu/jenkins', 'semver: ' + packagejson.version);
      cb();
    });
  });

  /* full deploy tasks */
  gulp.task('deploy', function(callback) {
    if(!argv.env) throw new Error('no env parameter !');
    if(argv.env === 'uat')  return runSequence('checkout-dev', 'bump', 'MEU', 'jenkins', 'checkout-dev', callback);
    if(argv.env === 'prod') return runSequence('checkout-uat', 'MEP', 'jenkins', 'checkout-dev', callback);
    if(argv.env === 'preprod') return runSequence('jenkins', 'checkout-dev', callback);
    throw new Error('bad env parameter !');
  });
} catch (e) {}
