$(function(){

    $('#approved').on('click', (function(e){
        e.preventDefault();

        var idReservation = $(this).attr('data-id');
        var url = $('#path_approbation').attr('data-id');

        $.ajax({
                type: 'POST',
                url: url,
                data: {'id' : idReservation, 'status' : 'valide'}
            })
            .done(function (data) {
                if (typeof data.success !== true) {
                    $(".button-approbation").hide();
                    $(".result-approbation").html('<p><i class="fa fa-check-circle fa-fw" style="color:$brand-primary"></i>Réservation validée</p>')
                }
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                if (typeof jqXHR.responseJSON !== 'undefined') {
                    console.log('Erreur lors de la requête ajax')
                } else {
                    alert(errorThrown);
                }
            });

    }));

    $('#refused').on('click', (function(e){
        e.preventDefault();
        var idReservation = $(this).attr('data-id');
        var url = $('#path_approbation').attr('data-id');

        $.ajax({
                type: 'POST',
                url: url,
                data: {'id' : idReservation, 'status' : 'refused'}
            })
            .done(function (data) {
                if (typeof data.success !== true) {
                    $(".button-approbation").hide();
                    $(".result-approbation").html('<p><i class="fa fa-minus-circle fa-fw" style="color:#db514f"></i>Réservation refusée</p>')
                }
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                if (typeof jqXHR.responseJSON !== 'undefined') {
                    console.log('Erreur lors de la requête ajax')
                } else {
                    alert(errorThrown);
                }
            });
    }));

});