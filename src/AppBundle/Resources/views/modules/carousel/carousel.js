
export default function carousel() {

  $(document).ready(function () {

      $('#carouselModal').on('show.bs.modal', function (e) {
          // if (!data) return e.preventDefault() // stops modal from being shown
          $('.owl-carousel').owlCarousel({
              loop: true,
              items: 1,
              nav: true,
              navText: ["<i class='fa fa-chevron-left' aria-hidden='true'></i>","<i class='fa fa-chevron-right' aria-hidden='true'></i>"]
          });

      });
  });

}

carousel.selector = '#carouselModal';
