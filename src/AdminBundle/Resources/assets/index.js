// Add client side javascript here
import bootstrap from '../../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap';

document.addEventListener('DOMContentLoaded', e => {

  // loads all components
  [
    bootstrap
  ].forEach(Component => {
    if (document.querySelector(Component.selector)) Component();
  });
});
