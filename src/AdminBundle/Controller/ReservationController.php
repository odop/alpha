<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use OdopData\Entity\Reservation;
use AdminBundle\Form\ReservationType;

/**
 * Reservation controller.
 *
 * @Route("admin/reservation")
 */
class ReservationController extends Controller
{
    /**
     * Lists all Reservation entities.
     *
     * @Route("/{page}", name="admin_reservation_index", defaults={"page": 1})
     * @Route("/{filter}/{page}", name="admin_reservation_index_by",
     *    requirements={"filter": "pending|valide|completed"},
     *    defaults={"page": 1})
     * @Method("GET")
     * @param string $filter
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, $filter = "", $page)
    {
        $this->get('admin.breadcrumbs')->setBreadcrumbs(array("admin_reservation_index" => "Réservations"));
        $em = $this->getDoctrine()->getManager();

        if(!$filter)
            $reservations = $em->getRepository(Reservation::class)->findAll();
        else
            $reservations = $em->getRepository(Reservation::class)->findBy(array('status' => $filter));

        $paginator  = $this->get('knp_paginator');
        $result = $paginator->paginate(
            $reservations, /* query NOT result */
            $request->query->getInt('page', $page)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('AdminBundle:Reservation:index.html.twig', array(
            'reservations' => $result,
        ));
    }

    /**
     * Creates a new Reservation entity.
     *
     * @Route("/new", name="admin_reservation_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $this->get('admin.breadcrumbs')->setBreadcrumbs(
            array("admin_reservation_index" => "Réservations",
                "admin_reservation_new" => "Nouveau" ));

        $reservation = new Reservation();
        $form = $this->createForm('AdminBundle\Form\ReservationType', $reservation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($reservation);
            $em->flush();

            $this->addFlash(
                'success',
                'Ajouté avec succès'
            );

            return $this->redirectToRoute('admin_reservation_show', array('id' => $reservation->getId()));
        }

        return $this->render('AdminBundle:Reservation:new.html.twig', array(
            'reservation' => $reservation,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Reservation entity.
     *
     * @Route("/{id}/show", name="admin_reservation_show")
     * @Method("GET")
     * @param Reservation $reservation
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Reservation $reservation)
    {
        $this->get('admin.breadcrumbs')->setBreadcrumbs(
            array("admin_reservation_index" => "Réservations",
                "admin_dashboard" => "Afficher" ));

        $deleteForm = $this->createDeleteForm($reservation);
        $form = $this->createForm('AdminBundle\Form\ReservationType', $reservation);

        return $this->render('AdminBundle:Reservation:show.html.twig', array(
            'reservation' => $reservation,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Reservation entity.
     *
     * @Route("/{id}/edit", name="admin_reservation_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Reservation $reservation
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Reservation $reservation)
    {
        $this->get('admin.breadcrumbs')->setBreadcrumbs(
            array("admin_reservation_index" => "Réservations",
                "admin_dashboard" => "Editer" ));

        $deleteForm = $this->createDeleteForm($reservation);
        $form = $this->createForm('AdminBundle\Form\ReservationType', $reservation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($reservation);
            $em->flush();

            $this->addFlash(
                'success',
                'Modifications enregistrées avec succès'
            );

            return $this->redirectToRoute('admin_reservation_edit', array('id' => $reservation->getId()));
        }

        return $this->render('AdminBundle:Reservation:edit.html.twig', array(
            'reservation' => $reservation,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Reservation entity.
     *
     * @Route("/{id}", name="admin_reservation_delete")
     * @param Request $request
     * @param Reservation $reservation
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Reservation $reservation)
    {
        $form = $this->createDeleteForm($reservation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($reservation);
            $em->flush();
        }

        $this->addFlash(
            'success',
            'Supprimé avec succès'
        );

        return $this->redirectToRoute('admin_reservation_index');
    }

    /**
     * Creates a form to delete a Reservation entity.
     *
     * @param Reservation $reservation The Reservation entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Reservation $reservation)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_reservation_delete', array('id' => $reservation->getId())))
            ->getForm()
        ;
    }
}
