
export default function homepage() {

  let startDate = $('.startDate');

  /**
   * Initialisation of datetimepicker on the current page
   */
  startDate.datetimepicker({
      locale: 'fr',
      //   disabledDates: {{ reservations|json_encode()|raw }},
      minDate: moment(),
      showClose: true,
  });

  startDate.on("dp.show", function (e) {
    $(".day").on('click', function(){
      $("a[data-action='togglePicker']").trigger('click');
    });
  });

  startDate.on("dp.change", function (e) {
      $(".day").on('click', function(){
        $("a[data-action='togglePicker']").trigger('click');
      });
  });

}

homepage.selector = '#quick-search';
