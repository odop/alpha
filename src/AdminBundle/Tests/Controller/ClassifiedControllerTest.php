<?php

namespace AdminBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ClassifiedControllerTest extends WebTestCase
{

    public function testCompleteScenario()
    {
        // Create a new client to browse the application
        $client = static::createClient(array(), array(
            'PHP_AUTH_USER' => 'julien.decoen@gmail.com',
            'PHP_AUTH_PW'   => 'mdp123',
        ));

        // Create a new entry in the database
        $crawler = $client->request('GET', '/admin/classified/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET /admin/category/");
        $crawler = $client->click($crawler->filter('.header a')->link());

        // Fill in the form and submit it
        $form = $crawler->selectButton('Ajouter')->form(array(
            'classified[title]'  => 'Test',
            'classified[country]'  => 'France',
            'classified[region]'  => 'Val doise',
            'classified[city]'  => 'Herblay',
            'classified[description]'  => 'Description test',
            'classified[maxParticipant]'  => '23',
            'classified[surfaceArea]'  => '23',
            'classified[price]'  => '23',
            'classified[categorys][1]'  => 4,
            // ... other fields to fill
        ));

        $client->submit($form);
        $crawler = $client->followRedirect();

        // Check data in the show view
        $this->assertGreaterThan(0, $crawler->filter('.alert-success:contains("Ajouté avec succès")')->count(), 'Fail to create new');

        // Edit the entity
        $crawler = $client->click($crawler->selectLink('Editer')->link());

        $form = $crawler->selectButton('Modifier')->form(array(
            'classified[title]'  => 'Test Edit',
            // ... other fields to fill
        ));

        $client->submit($form);
        $crawler = $client->followRedirect();

        // Check the element contains an attribute with value equals "Foo"
        $this->assertGreaterThan(0, $crawler->filter('.alert-success:contains("Modifications enregistrées avec succès")')->count(), 'Fail to edit');

        // Delete the entity
        $client->submit($crawler->filter('#deleteModal')->selectButton('Supprimer')->form());
        $crawler = $client->followRedirect();

        // Check the entity has been delete on the list
        $this->assertGreaterThan(0, $crawler->filter('.alert-success:contains("Supprimé avec succès")')->count(), 'Fail to delete');

    }

}
